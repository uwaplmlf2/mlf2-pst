#
# $Id: SConstruct,v c75c685602ce 2008/08/27 04:22:56 mikek $
#
# Scons build script for MLF2 mission software
#
import os
from subprocess import Popen, PIPE


def get_build_id():
    """Get the Git commit ID"""
    cmdline = 'git log -n 1 --pretty=oneline'
    output = Popen(cmdline.split(), stdout=PIPE).communicate()[0]
    return (output.strip().split())[0]


def generate_build_id(env, target, source):
    out = open(target[0].path, 'w')
    build_id = source[0].get_contents()
    out.write('#define REVISION "%s"\n' % build_id)
    out.close()


def read_devices(filename):
    f = open(filename, 'r')
    d = {}
    for line in f:
        fields = line.strip().split()
        key = 'HAVE_' + fields[0].upper()
        try:
            d[key] = fields[1]
        except IndexError:
            d[key] = ''
    return d


def get_float_type():
    dirname = os.path.basename(os.getcwd())
    try:
        return dirname.split('-')[1]
    except IndexError:
        return dirname

env = Environment(tools=['default', 'crossgcc', 'mkheader', 'mkarchive', 'findparams',
                         'combineparams'])
devfile = ARGUMENTS.get('devfile', 'DEVICES')
header_defs = read_devices(devfile)
mtype = ARGUMENTS.get('mtype', 'mlb')
header_defs[mtype.upper()] = ''
env['HEADER_DEFS'] = header_defs

vers = env.Command('#version.h', [Value(get_build_id())], generate_build_id)
hdr_config = env.MakeHeader('config.h.in')
hdr_mtype = env.MakeHeader('mtype.h.in')
env.Alias('headers', [hdr_config, hdr_mtype, vers])

env['MTYPE'] = mtype
env['FLOAT_TYPE'] = get_float_type()
try:
    env['FLOAT_SUBTYPE'] = devfile.split('-')[1]
except IndexError:
    env['FLOAT_SUBTYPE'] = ''

env['LINKFILE'] = 'tt8-ram.ld'
env['MLF2INCLUDE'] = os.environ.get('MLF2INCLUDE', '/usr/local/mlf2/include')
env['MLF2LIBDIR'] = os.environ.get('MLF2LIBDIR', '/usr/local/mlf2/lib')
env['TT8INCLUDE'] = os.environ.get('TT8INCLUDE', '/usr/local/mlf2/include')
env['TT8LIBDIR'] = os.environ.get('TT8LIBDIR', '/usr/local/mlf2/lib')
env.Append(CPPPATH=['#', '#ballast', '#mission', env['MLF2INCLUDE'], env['TT8INCLUDE']])
env.Append(CFLAGS=Split('-DNDEBUG=1 -Wall -Wstrict-prototypes'))

env['PROGDIR'] = '#.run'
env['ARCHIVE_DEST'] = 'sigma.apl.washington.edu:public_html/mlf2dist'
env['PTABLE_DEST'] = 'sigma.apl.washington.edu:/var/mlf2/params'
env['PTABLE_XSL'] = '/usr/local/mlf2/etc/params.xsl'
SConscript('SConscript', build_dir='.build',
           duplicate=0,
           exports={'env': env})
