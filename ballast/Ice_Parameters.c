Ice.IntervalSec = 3600; // burst interval
Ice.DurationSec = 500; // burst duration
Ice.DelaySec = 3600;		// delay before the first burst (after start of a mode, check against mode_time)
Ice.PatmUsed = 9.90;			// Patm used by the altimeter, must match surfcheck.c!
Ice.Patm = 10.0;			// Best guess of the actual Patm
Ice.IceReturnScale = 10.0;  // Scaling used to return ice draft from surfcheck  (must match surfcheck.c!)
Ice.SurfcheckRequested = 0; // Set internally to signal that surfcheck has been requested and we should expect surfcheck result

Ice.MaxPhotoP = 16.5; // max depth to take photos

#ifdef SIMULATION
Ice.EpochYearDay[0] = yday + 0.5;// 243;//  1 Sep.
Ice.EpochYearDay[1] = yday + 1;//250;//  8 Sep.
Ice.EpochYearDay[2] = yday + 1.5;//257;// 15 Sep.
#else
Ice.EpochYearDay[0] = 243;//  1 Sep.
Ice.EpochYearDay[1] = 250;//  8 Sep.
Ice.EpochYearDay[2] = 257;// 15 Sep.
#endif

Ice.IgnoreBad[0] = 0; // Ignore bad returns? (otherwise, each one resets the GreenCount)
Ice.IgnoreBad[1] = 0;
Ice.IgnoreBad[2] = 0;

Ice.NeedGreenCount[0] = 2; // min. number of "green flags" to surface?
Ice.NeedGreenCount[1] = 1;
Ice.NeedGreenCount[2] = 0;

Ice.SurfaceEarly[0] = 0; // whether to surface as soon as the surfacing rule is satisfied (otherwise let the mode time out)
Ice.SurfaceEarly[1] = 0;
Ice.SurfaceEarly[2] = 0;



ADD_PTABLE_L(   Ice.IntervalSec  );
ADD_PTABLE_L(   Ice.DurationSec  );
ADD_PTABLE_L(   Ice.DelaySec  );
ADD_PTABLE_D(   Ice.PatmUsed  );
ADD_PTABLE_D(   Ice.Patm  );
ADD_PTABLE_D(   Ice.IceReturnScale  );
ADD_PTABLE_S(   Ice.SurfcheckRequested  );

ADD_PTABLE_D(   Ice.MaxPhotoP  );

ADD_PTABLE_D(   Ice.EpochYearDay[0]  );
ADD_PTABLE_D(   Ice.EpochYearDay[1]  );
ADD_PTABLE_D(   Ice.EpochYearDay[2]  );

ADD_PTABLE_S(   Ice.IgnoreBad[0]  );
ADD_PTABLE_S(   Ice.IgnoreBad[1]  );
ADD_PTABLE_S(   Ice.IgnoreBad[2]  );

ADD_PTABLE_S(   Ice.NeedGreenCount[0]  );
ADD_PTABLE_S(   Ice.NeedGreenCount[1]  );
ADD_PTABLE_S(   Ice.NeedGreenCount[2]  );

ADD_PTABLE_S(   Ice.SurfaceEarly[0]  );
ADD_PTABLE_S(   Ice.SurfaceEarly[1]  );
ADD_PTABLE_S(   Ice.SurfaceEarly[2]  );
