/*****************SPURS2***************************/
/* This file is included in ballast.c 
   It sets initial values of all control variables, so that the main code
   does not need to be modified when values change */

// June 2016	SPURS 2

/* Do not change this variable */
static const char *__id__ = "$Id$";


# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN  900  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 65 /* Number of real commands  1 2 3 ... NCOMMAND */
# define LOGDAYSSIM 10  /* Stop ballastlog after this many days in simulation */

/* Declare misc. local scalar parameters - all satellite settable.  Actual initialization is in initialize_mission_parameters()*/
static double Mass0 ;  /* initial mass */
static double Creep;  /* kg/day */
static double deep_control;
static double shallow_control;  /*  m^2/dbar */
static double bottom_P;  /* Depth to push out piston */
static double error_P; /* Depth to declare emergency */
static double top_P;   /* Minimum allowed depth */
short int stage;   /* Master mode control variable */
static double telem_Step;   /* Commanded change in target */
static double Home_days;    /* Home about this often */
static double MaxQuietDays; /*maximum number of days without a comm mode - ERROR - safety feature */
static double sleepduration;// Total sleep time sec <<<< This should be a Sleep structure, perhaps

static unsigned long State;  // this is a combination of Stage, icall(phase), Mode, and Variation bytes. E.g., "Variation" can indicate the dritf "flavor" (ISO/SEEK/ML, so we could get rid of those modes), or whether SSAL is active, or stuff like that.
#define SET_STATE(s,p,m,v) State=((s<<24)|(p<<16)|(m<<8)|(v))
#define SET_STATE_STAGE(s) State=State^((State^(s<<24))&(0xFF<<24))
#define SET_STATE_PHASE(p) State=State^((State^(p<<16))&(0xFF<<16))
#define SET_STATE_MODE(m)  State=State^((State^(m<<8))&(0xFF<<8))
#define SET_STATE_VAR(v) State=State^((State^(v))&0xFF)


/**************************************************************************************************
        Declare global mission-specific parameters - Tier II
****************************************************************************************************/

/* Initialize parameter values and link them with the Ptable */

static struct psballast {
	struct profile Down[2];
	struct settle Settle[2];
	struct drift Drift[2];
	struct profile Up;
} PSBallast;



static struct steps{
    // consists of up-comm-down-4xSettle cycle:
    struct profile Up;
    struct profile Down;
    // and 4 settles (which can be different!)
    struct settle Settle[4];
	short SetFromDepth; // whether to obtain target density for settles from the Settle[x].Ptarget (1) or specify directly as Settle[x].Target (0)
} Steps;

static struct bsteps {
	// consists of up-comm-down-4xBocha cycle:
	struct profile Up;
	struct profile Down;
	// and 4 constant-Bocha settles (which can be different!)
	struct bocha Bocha[4];
} BSteps;

static struct turb{
    // consists of Up-Comm-Down-Up-Drift cycle:
    struct profile Up;
    struct profile Down;
    // another up (insertion):
    struct profile InsUp;
	struct drift Drift;
} Turb;

static struct cycle{
    // consists of up-comm-down cycle:
    struct profile Up;
    struct profile Down;
    short ncycles;  // number of cycles before COMM
} Cycle;


/* Initialize mission-specific sampling control structures (for SSAL, ADCP, etc.)
   They will be used in Sample*.c
*/
static struct  ssal {
	// Surface salinity control 
	double onDepth;	// Run SSAL once we get shallower than this (starting with overlap). NB: we can disable SSAL by setting onDepth to -100!
	double offDepth;	// Depth to switch to regular CTD on the way down (i.e., in the mode with SSAL.Run=0)
	double OverlapTime; // seconds, duration of SSAL-CTD overlap mode 
	// private parameters for internal record-keeping (not exposed via Ptable)
	short Run;		// whether to attempt to run SSAL. Set by nextmode for the appropriate profiling modes. Whether the SSAL is actually running at each data cycle is determined by other parameters
} SSAL; 
static struct  nortekadcp {
	// Nortek ADCP control
	short Run;		// whether to run ADCP
} NortekADCP;


void initialize_mission_parameters(void){
    
    int k;

	struct profile Prof_Up, Prof_Down, Prof_Up_Fast, Prof_Down_Fast; // these are shorthand for the UP/DOWN profile defaults (they are only used to initialize the different Profile structures)

    /*******************************************/
    /* Sampling intervals
    ** Note that for the profiles, the "sampling interval" is now
    ** used to specify the maximum time allowed for ballast adjustment
    ** during each sample.
    */
    Si[MODE_PROFILE_UP] = 30;
    Si[MODE_PROFILE_DOWN] = 30;
  
	Si[MODE_SETTLE] = 30;
    Si[MODE_DRIFT_ISO] = 30;
    Si[MODE_DRIFT_ML] = 30;
    Si[MODE_DRIFT_SEEK] = 30;
    Si[MODE_SERVO_P] = 30;
    Si[MODE_BOCHA] = 30;

	/************************************************************************************************
	Initialization of global (external) defaults
	*************************************************************************************************/
	set_param_int("down_home", 1);	// Always home at the end of DOWN
	// set_param_int("aux:xfer_tmax", 600); // (MIZ) AUX file transfer timeout
    
    /************************************************************************************************
    Initialization of local static defaults
    ************************************************************************************************/
    deep_control = 25.e-6;
    shallow_control = 50.e-6;  /*  m^2/dbar */
    bottom_P = 150.;  /* Depth to push out piston */
    error_P = 220.; /* Depth to declare emergency */
    top_P = -100;   /* Minimum allowed depth */
    stage = 0;   /* Master mode control variable */
    telem_Step = 0.1;   /* Commanded change in target */
    Home_days = 1;    /* Home about this often */
    MaxQuietDays = 2; /*maximum number of days without a comm mode - ERROR - another safety feature */
    sleepduration = 8000;// Total sleep time sec

	/* ------------------ EOS PARAMETERS --------------  */

	EOS.Mass0 = 50.;  /* initial mass  (kg) */
	EOS.V0 = 48600.0e-6; /* Estimated float volume at B=0, P=Pair, T=Tref, no air (m^3)*/
	EOS.Air = 15.5e-6;      /* Air volume at the surface (m^3)*/
	EOS.Compress = 3.1e-6;  /* float compressibility (dbar^-1)*/
	EOS.Thermal_exp = 0.722e-4;     /* float thermal expansion coeff */
	EOS.Tref = 8;      /* reference temperature */
	EOS.Creep = 0.e-6;  /* kg/day */
	EOS.CreepStart = 0; /* Days since the start of the mission to start the Creep */
	EOS.Pair = 10.;     /* Atmospheric pressure - nominally 10 (dbar)*/

	/* ------------------ Error Control PARAMETERS --------------  */
    Error.Modes = -1;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    Error.Pmin = -1000;      /* Minimum Pressure  allowed */
    Error.Pmax = 1000;     /* Maximum Pressure  allowed */
    Error.timeout = 3600;    /* time outside of this range before error is declared*/

	/* ------------------ CTD PARAMETERS --------------  */
	CTD.which = MEANCTD;  /* How to get one CTD value from two CTD's - BOTTOMCTD ,TOPCTD, MEANCTD or MAXCTD */
	CTD.BadMax = -1;    /*How many bad CTDs before error   Long integer - Can be up to 2.1e9; -1 to disable*/
	CTD.Ptopmin = 2.1;      /* TopCTD not good above this pressure */
	CTD.Poffset = 0.106;      /* Offset for Pressure to be zero when top of float is at surface, adjusts pressure to be that at middle of float hull*/
	CTD.Separation = 1.42;  /* Distance between two CTDs*/
	CTD.TopSoffset = 0;    /* Correction to top CTD salinity */
	CTD.TopToffset = 0;   /*Correction to top CTD temperature*/
	CTD.BottomSoffset = 0;   /* Correction to Bottom CTD salinity */
	CTD.BottomToffset = 0;   /*Correction to Bottom CTD temperature*/

	/* ------------------ Mixed layer PARAMETERS --------------  */
	Mlb.go = 1;  /* 1 to compute MLB target, otherwise don't */
	Mlb.record = 0;   /* 1 to record data in arrays; else don't */
	Mlb.point = 0;       /* points at next open element in raw arrays, 0-> empty */
	Mlb.Nmin = 45;    /* minimum number of data to do computation */
	Mlb.dP = 2;     /* grid spacing m */
	Mlb.dSig = 0.02;  /* bin size for Sigma search */
	Mlb.Sigoff = 0.2; /* Offset of goal from ML density, final target = MLsigma + Sigoff */
					  //Mlb.Psave = 0; /* array of raw pressure */
					  //Mlb.Sigsave =0 ; /* array of raw potential density */
					  //Mlb.Pgrid = 0; /* array of gridded pressure */
					  //Mlb.Siggrid = 0; /* array of gridded potential density */

	/* ------------------ Bug control PARAMETERS --------------  */
	Bugs.start = -1;  /* sunset  / seconds of GMT day 0-86400 */
	Bugs.stop = -1;  /* sunrise */
	Bugs.start_weight = 0;  /* sunset weight / kg  */
	Bugs.stop_weight = 0;  /* sunrise weight /kg - linear interpolation between */
	Bugs.flap_interval = 10000;  /* time between flaps / seconds */
	Bugs.flap_duration = 130;  /* time between close and open */
	Bugs.weight = 0; /* weight - don't set  */

	/* ------------------ Timer PARAMETERS --------------  */
	Timer.enable = 1;       // ==1 if all timers are enabled (individual timers can be controlled by setting to -1  - This master is turned on/off in mission code to suppress timers)
	Timer.time[0] = -1;  // time (in seconds since midnight GMT) for the first timer event.
	Timer.time[1] = -1;       // ... for the second timer event
	Timer.time[2] = -1;       // ... for the third timer event
	Timer.time[3] = -1;       // ... for the fourth timer event
	Timer.command[0] = -1;    // timer command to issue. Usually, this is a stage number
	Timer.command[1] = -1;   // ... second timer
	Timer.command[2] = -1;   // ... third timer
	Timer.command[3] = -1;   // ... fourth timer
	Timer.nskip[0] = 0;   // How many timer triggers to skip? For multiday timing.
	Timer.nskip[1] = 0;   // timer2
	Timer.nskip[2] = 0;   // timer3
	Timer.nskip[3] = 0;   // timer4

	for (k = 0; k < NTIMERS; k++)
		Timer.countdown[k] = Timer.nskip[k]; // DON'T SET - internal countdown variable



    /************************************************************************************************
         Initialization of Tier 1 (default) mode parameters to serve as default templates
    *************************************************************************************************/

	// Shorthand templates for the UP/DOWN profile defaults (they are only used to initialize the different Profile structures)
	// First, profiles with speed control:
	Prof_Up.timeout	= 3600;		// Max duration 
	Prof_Up.Ptarget	= 3;		// Pressure to end the profile at
	Prof_Up.ExtraTime = 180;	// time/sec past reaching Prof.Ptarget before end
	Prof_Up.deltaB = 100e-6;	// Bocha offset relative to the local equilibrium (proxy for profiling speed). 
	Prof_Up.Extrap = 1;			// Extrapolation parameter for predicting B0. =0 disables extrapolation, =1 extrapolates to the next data cycle, =0.5 to half-way
	Prof_Up.Bmin = 0;		// min bocha setting
	Prof_Up.Bmax = 600e-6;		// max bocha setting
	Prof_Up.drogue = 0;		// drogue open (1) or closed (0) during the profile
	Prof_Up.Speedbrake = 0;  // if 1, use drogue as speedbrake until float moves in the right direction
	
	Prof_Down.timeout = 3600;		// Max duration 
	Prof_Down.Ptarget = 120;		// Pressure to end the profile at
	Prof_Down.ExtraTime = 0;	// time/sec past reaching Prof.Ptarget before end
	Prof_Down.deltaB = -100e-6;		// Bocha offset relative to the local equilibrium (proxy for profiling speed). 
	Prof_Down.Extrap = 1;			// Extrapolation parameter for predicting B0. =0 disables extrapolation, =1 extrapolates to the next data cycle, =0.5 to half-way
	Prof_Down.Bmin = 0;		// min bocha setting
	Prof_Down.Bmax = 600e-6;		// max bocha setting
	Prof_Down.drogue = 0;		// drogue open (1) or closed (0) during the profile
	Prof_Down.Speedbrake = 0;	// if 1, use drogue as speedbrake until float moves in the right direction
	
	// Next, full-speed profiles:
	Prof_Up_Fast = Prof_Up;
	Prof_Up_Fast.deltaB = 1;

	Prof_Down_Fast = Prof_Down;
	Prof_Down_Fast.deltaB = -1;

	// Real Tier 1 defaults start here:
	Prof = Prof_Up;
    
    Servo_P.Pmin=5;   /* end if it goes above this depth */
    Servo_P.Pmax=100; /* end if it goes below this depth */
    Servo_P.Pgoal=6;  /* pull in Bocha if shallower than this*/
    Servo_P.Bspeed=0.1e-6; /* bocha increase speed when below Pgoal, m^3/s */
    Servo_P.BspeedEtime=1.e7; /* efolding time to decrease Bspeed (sec) */
    Servo_P.BspeedShallow=2.e-7;  /* bocha reduction speed when above Pgoal, m^3/s*/
    Servo_P.timeout=2000;  /* timeout seconds */
    Servo_P.drogue=0;      /* drogue position: 0 closed, 1 open */

    Settle.secOday = -1;   /* end Settle when time passes this clock time [0 86400] (obsolete: replaced by timers) */
    Settle.timeout = 7200;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    Settle.seek_time = 0.8;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    Settle.decay_time = 0.4;      /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    Settle.nav = 20;                /* number of points to average to get settled volume */
	Settle.drogue_time = 0;// 300.; /* time after start of settle to keep drogue open */
    Settle.beta = 2; /* pseudo compressive gain (big for stable)*/
    Settle.tau = 400;             /* seek gain (big for stable)*/
    Settle.weight_error = 1;    /* Accept seek values if Dsig*V0 is     less than this (in kg)*/
    Settle.Vol_error = 0.5e-6;       /* 0.5e-6 Accept volume if stdev of V0 is less     than this ( if <0 no volume set) */
    Settle.Ptarget = 15;      /* Target Pressure - used only with Nfake or in Steps*/
    Settle.Nfake = 0;                   /* Fake stratification relative to Ptarget  No effect if 0 */
    Settle.nskip = 1;           /*      (if 0, skip settle mode always)  */
    Settle.SetTarget = 0; /* How to set target:  1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    Settle.Target = 1022.4;          /* Settle Target isopycnal */
    Settle.B0 = 50e-6;              /* bocha,  don't need to set */
    Settle.Bmin = 0.e-6; /* Minimum Bocha */
    Settle.Bmax = 650.e-6; /* Maximum Bocha */
	Settle.SetV0 = 0; /* if ==1, set EOS.V0 after the settle */

    Bocha.B = 150.e-6; /* Constant bocha to set*/
    Bocha.Btol = -1; /* Tolerance of Bocha. Set to -1 to disable exit-on-target-B */
    Bocha.timeout = 600.; /* Max duration sec*/
    Bocha.drogue = 0;    /* 0 or 1 - drogue open or closed  */
	Bocha.SetV0 = 0; /* if ==1, set EOS.V0 after the settle */

    Drift.SetTarget = 3; /*  How to set Target:     1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    Drift.VoffZero = 1;  /* 1 to set Voff=0 at drift start, else keep value */
    Drift.median = 1;  /* 1 use 5 point median filter, 0 don't */
    Drift.timetype = 2; /* How to end Drift mode 1: Use time since end of last drift mode or mission start 2: At the given timeOday (defaults to noon = 0.5) - Obsolete - Use Timers instead */
    Drift.time_end_sec = -1;   /* time/seconds to end Drift mode */
    Drift.timeout_sec = 50000.;  /* Additional timeout to end mode / seconds*/
    Drift.Voff = 0;       /* Ballast adjustment during Drift  */
    Drift.Voffmin = -200e-6;  /*prevent negative runaway on bottom */
    Drift.Target = 1024.0;         /* target isopycnal */
    Drift.iso_time = 7200;        /*seek time toward surface  sec  */
    Drift.seek_Pmin = -100;      /* depth range for drift seek mode*/
    Drift.seek_Pmax = -50;      /* continued */
    Drift.iso_Gamma = 1;   /*Pseudo-compressibility  m^3/unit, 1 = isopycnal*/
    Drift.time2 = 600;     /* second timeout parameter -- compatibility only, DO NOT USE  */
	Drift.closed_time = 100000;// 200;   /* drogue opens after this time   sec  */
	Drift.SetV0 = 0; /* if ==1, set EOS.V0 after the drift */

    Ballast.SetTarget = 1;  /* how to set Ballast.Target    1,2,3-no changes,    4- end of good Settle      5 - end of Drift, 6-end of Down, 7-end of Up,   8- Value at P=Ballast.Pgoal     9- Value from GetMLB (not implemented yet) */
    Ballast.MLsigmafilt = 0;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    Ballast.MLthreshold = -100;   /* Switch to ML ballasting if P<Pdev*[]     */
    Ballast.MLmin = 2;            /*        or if P<     */
    Ballast.MLtop = 2;          /* or between top and bottom */
    Ballast.MLbottom =10;
    Ballast.SEEKthreshold = -100;   /* Allow isoSEEK if P>Pdev*[]  & Pressure between Drift.seek_* limits   */
	Ballast.T0 = 26.8;      /* most recent temperature at ballasted point */
    Ballast.S0 = 37.45;      /* most recent Salinity  */
    Ballast.P0 = 0;      /* most recent Pressure*/
    Ballast.rho0 = 1024.628;    /* most recent water (=float) density (used to be important)*/
    Ballast.B0 = 160.e-6;      /* most recent bocha setting at ballasted point */
    Ballast.TH0 = 26.8;     /* most recent potential temperature */
    Ballast.Vdev = 0;  /* most recent stdev of float volume estimates */
    Ballast.Pgoal = 30;  /* if STP==2, set STP at this depth */
    Ballast.Target = 1022.4; /* target isopycnal e.g. 1022 */

 
/************************************************************************************************
Initialization of Tier 2 parameters.
These determine variations of the Mode parameters above for each particular stage
*************************************************************************************************/
/* ------------------ PS BALLAST STAGE PARAMETERS --------------  */
	PSBallast.Down[0] = Prof_Down_Fast;
	PSBallast.Down[0].Ptarget = 60;	// Depth of the first (captive) dive & drift

	PSBallast.Settle[0] = Settle;
	PSBallast.Settle[0].timeout = 300;	// length of the first (captive) settle - typically, very short 
	PSBallast.Settle[0].SetTarget = 1; // set settle target at end of dive -- this is where it settles 
	PSBallast.Settle[0].SetV0 = 0; // do not set EOS.V0 based on the first (captive) settle
	

	PSBallast.Drift[0] = Drift;
	PSBallast.Drift[0].timeout_sec = 100;	// length of the first (captive) drift - typically, very short 
	PSBallast.Drift[0].closed_time = PSBallast.Drift[0].timeout_sec + 100.; // don't open drogue in captive dive 

	PSBallast.Down[1] = Prof_Down_Fast;
	PSBallast.Down[1].Ptarget = 60;	// Depth of the second (longer) dive & drift

	PSBallast.Settle[1] = Settle;
	PSBallast.Settle[1].timeout = 10000;	// length of the second (longer) settle 
	PSBallast.Settle[1].seek_time = 8000;	// length of seeking time during settle
	PSBallast.Settle[1].SetTarget = 1; // set settle target at end of dive -- this is where it settles 
	PSBallast.Settle[1].SetV0 = 1; // DO set EOS.V0 after the second settle

	PSBallast.Drift[1] = Drift;
	PSBallast.Drift[1].timeout_sec = 1000;	// length of the second (longer) drift. 
	PSBallast.Drift[1].closed_time = PSBallast.Drift[1].timeout_sec / 5.; //open part way through
    
	PSBallast.Up = Prof_Up_Fast;

/* ------------------ STEPS STAGE PARAMETERS --------------  */
    Steps.Up = Prof_Up_Fast;             // Start with the default Up and Down
    Steps.Down = Prof_Down_Fast;
    Steps.Down.Ptarget = 80;   // This is automatically increased to max of Steps.Settle[].Ptarget + 1
	
    Steps.Settle[0] = Settle;    // Copy Settle to Steps.Settle
    Steps.Settle[1] = Settle;
    Steps.Settle[2] = Settle;
    Steps.Settle[3] = Settle;
    Steps.Settle[0].drogue_time=0;   // No need to open drogue at start of each step
    Steps.Settle[1].drogue_time=0;
    Steps.Settle[2].drogue_time=0;
    Steps.Settle[3].drogue_time=0;
	
	Steps.SetFromDepth = 1; // whether to obtain target density for settles from the Settle[x].Ptarget (1) or specify directly as Settle[x].Target (0)
	Steps.Settle[0].Ptarget = 90;     // First settle DEPTH (use with Steps.SetFromDepth == 1)
	Steps.Settle[1].Ptarget = 70;		// second
	Steps.Settle[2].Ptarget = 50;		// etc.
	Steps.Settle[3].Ptarget = 30;
//	Steps.Settle[0].Target = 1026;		// First settle DENSITY (use with Steps.SetFromDepth == 0)
//	Steps.Settle[1].Target = 1025;		// second
//	Steps.Settle[2].Target = 1024;		// etc.
//	Steps.Settle[3].Target = 1022;

    
//    Steps.Settle[3].beta=1;               // Shallowest settle has only a weak seeking in expected strong stratification
//    Steps.Settle[3].tau=1000;
    
    Steps.Settle[0].timeout=10000;     // Settle times
    Steps.Settle[1].timeout=10000;
    Steps.Settle[2].timeout=10000;
    Steps.Settle[3].timeout=50000;     // This is very long - will be interupted by the timer

/* ------------------ B-STEPS STAGE PARAMETERS --------------  */
	BSteps.Up = Prof_Up_Fast;             // Start with the default Up and Down
	BSteps.Down = Prof_Down_Fast;
	
	BSteps.Bocha[0] = Bocha;    // Copy Bocha to BSteps.Bocha
	BSteps.Bocha[1] = Bocha;
	BSteps.Bocha[2] = Bocha;
	BSteps.Bocha[3] = Bocha;
	BSteps.Bocha[0].B = 185e-6;	// Bocha targets
	BSteps.Bocha[1].B = 200e-6;
	BSteps.Bocha[2].B = 250e-6;
	BSteps.Bocha[3].B = 300e-6;
	BSteps.Bocha[0].timeout = 10000;     // Settle times
	BSteps.Bocha[1].timeout = 10000;
	BSteps.Bocha[2].timeout = 10000;
	BSteps.Bocha[3].timeout = 50000;     // This is very long - will be interupted by the timer


/* ------------------ TURBULENCE STAGE PARAMETERS --------------  */
    Turb.Up = Prof_Up_Fast;
    Turb.Down = Prof_Down_Fast;
    Turb.InsUp = Prof_Up; // Special Insertion Up
	Turb.Drift = Drift;

    Turb.Down.Ptarget = 20; // Insertion depth + about 10m

	Turb.InsUp.Ptarget = 15.;            // Insertion depth    
    Turb.InsUp.ExtraTime = 0;
    Turb.InsUp.deltaB = 20e-6;       // slower insertion
    Turb.InsUp.timeout = 3000.;      //  Adjusted to get float to the right place before ending up
    

    /* ------------------ PROFILE STAGE PARAMETERS --------------  */
 Cycle.Up=Prof_Up;
 Cycle.Down=Prof_Down;
    
 Cycle.Down.Ptarget=100; // Dive to this depth
 Cycle.Up.Ptarget = 3; // Go up to this depth
 Cycle.Up.ExtraTime= 0.;  /* time/sec past reaching up.Pend before end */
 
    
 Cycle.ncycles=4;    /* skip this many COMMs on surfacing */

/* ------------------ SSAL CONTROL PARAMETERS --------------  */
	SSAL.onDepth = 30;	// Run SSAL once we get shallower than this (starting with overlap)
	SSAL.offDepth = 1;	// Depth to switch to regular CTD on the way down (i.e., in the mode with SSAL.Run=0)
	SSAL.OverlapTime = 120; // seconds, duration of SSAL-CTD overlap mode 
	SSAL.Run = 0;		// whether to attempt to run SSAL. Set by nextmode for the appropriate profiling modes. Whether the SSAL is actually running at each data cycle is determined by other parameters

/* ------------------ NortekADCP CONTROL PARAMETERS --------------  */
	NortekADCP.Run = 1;		// whether to run ADCP


/************************************************************************************************
	Make Ptable links for mission-specific parameters defined above
*************************************************************************************************/

    /* Make Ptable links to the Mission-specific (Tier 2) parameters */
    /* These macros expand to add_param("PSBallast.depth1",	PTYPE_DOUBLE, &PSBallast.depth1), etc. */
    /* Use ADD_PTABLE_D for PTYPE_DOUBLE, ADD_PTABLE_L for PTYPE_LONG, and ADD_PTABLE_S for PTYPE_SHORT.*/
    
    /* PS Ballast */
	ADD_PTABLE_PROFILE(	PSBallast.Down[0]	);
	ADD_PTABLE_PROFILE(	PSBallast.Down[1]	);
	ADD_PTABLE_SETTLE(	PSBallast.Settle[0]	);
	ADD_PTABLE_SETTLE(	PSBallast.Settle[1]	);
	ADD_PTABLE_DRIFT(	PSBallast.Drift[0]	);
	ADD_PTABLE_DRIFT(	PSBallast.Drift[1]	);
	ADD_PTABLE_PROFILE(	PSBallast.Up		);

	
	/* Steps */
	ADD_PTABLE_S(		Steps.SetFromDepth	);
	ADD_PTABLE_PROFILE(	Steps.Down	);
	ADD_PTABLE_PROFILE(	Steps.Up	);
	ADD_PTABLE_SETTLE(	Steps.Settle[0]	);
	ADD_PTABLE_SETTLE(	Steps.Settle[1]	);
	ADD_PTABLE_SETTLE(	Steps.Settle[2]	);
	ADD_PTABLE_SETTLE(	Steps.Settle[3]	);

	/* B-Steps */
	ADD_PTABLE_PROFILE(	BSteps.Down		);
	ADD_PTABLE_PROFILE(	BSteps.Up		);
	ADD_PTABLE_BOCHA(	BSteps.Bocha[0]	);
	ADD_PTABLE_BOCHA(	BSteps.Bocha[1]	);
	ADD_PTABLE_BOCHA(	BSteps.Bocha[2]	);
	ADD_PTABLE_BOCHA(	BSteps.Bocha[3]	);

    /* Turbulence */
	ADD_PTABLE_PROFILE(	Turb.Down	);
	ADD_PTABLE_PROFILE(	Turb.Up	);
	ADD_PTABLE_PROFILE(	Turb.InsUp	);
	ADD_PTABLE_DRIFT(	Turb.Drift	);
    
    /* Cycle */
    ADD_PTABLE_PROFILE(	Cycle.Down		);
    ADD_PTABLE_PROFILE(	Cycle.Up		);
    ADD_PTABLE_S(		Cycle.ncycles	);
    
	/* SSAL */
	ADD_PTABLE_D(		SSAL.onDepth	);
	ADD_PTABLE_D(		SSAL.offDepth	);
	ADD_PTABLE_D(		SSAL.OverlapTime);

	/* NortekADCP */
	ADD_PTABLE_S(		NortekADCP.Run	);

};
