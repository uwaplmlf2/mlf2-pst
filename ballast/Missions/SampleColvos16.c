/*****************SPURS2***************************/
/* Sampling subroutine: Called on every data loop to set float sampling scheme for this mission */
 
// June 2016 - SPURS2

#ifdef SIMULATION
/* Surface Salinity (Seabird STS) control dummy plugs */
void* ssal_start_profile(long timeout) {
	return (void*)1;
}

int ssal_stop_profile(void *obj) {
	return 1;
}
#endif 

int sampling(int nmode, double day_time, double day_sec, double mode_time)
{
	static void* ssal = NULL; // surface salinity control object
	static double ProfileStartTime = 0; // store the time a profile was started

	int result;

	if (SSAL.Run ) { // SSAL is potentially active in this mode!
		if (ProfileStartTime == 0 && PressureG<SSAL.onDepth) { //not yet running, but the onDepth has been reached - start up
			ProfileStartTime = day_time;
			ENABLE_SENSORS(nmode, SENS_SSAL_CAL);
			log_event("%10.5f P %3.1f SSAL Cal\n",day_time,PressureG);
			SET_STATE_VAR(1); // mark a mode variation
		}
		if (ssal == NULL && ProfileStartTime>0 && (day_time - ProfileStartTime)>SSAL.OverlapTime / 86400.) {		// SSAL was active for more than SSAL.OverlapTime (in overlap mode), now start the actual profile
			ssal = ssal_start_profile((long)SSAL.Timeout); 
			if (ssal != NULL) {
				log_event("%10.5f P %3.1f SSAL Start\n", day_time, PressureG);
				SET_STATE_VAR(2); // mark a mode variation
			}
			else {
				log_event("SSAL ERROR\n");
			}
		}
	}
	else if (ssal != NULL && PressureG>SSAL.offDepth) { // SSAL should not be active, but it is still running and we've reached the offDepth  -- shut it down
		result = ssal_stop_profile(ssal);
		ssal = NULL;
		ProfileStartTime = 0;
		log_event("%10.5f P %3.1f SSAL end\n", day_time, PressureG);
	}

return(0);
}
