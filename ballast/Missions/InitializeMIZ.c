/* This file is included in ballast.c */
/* It sets initial values of all control variables, so that the main code
   does not need to be modified when values change */
/* Do not change this variable */
static const char *__id__ = "$Id$";
#include <sys/types.h>
#include <time.h>

/* Oct 2014 - ORCA - Massive simplification of this code since mission is so simple
   No Level II parameters */
// 25 July 2012 - separate parameter declaration (global) and initialization(in setup() function), in anticipation of the two-tier parameter model



# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN  (-1)  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 65 /* Number of real commands  1 2 3 ... NCOMMAND */
# define LOGDAYSSIM 10  /* Stop ballastlog after this many days in simulation */

/* Declare misc. local scalar parameters - all satellite settable.  Actual initialization is in initialize_mission_parameters()*/
static double Mass0 ;  /* initial mass */
static double Creep;  /* kg/day */
static double deep_control;
static double shallow_control;  /*  m^2/dbar */
static double bottom_P;  /* Depth to push out piston */
static double error_P; /* Depth to declare emergency */
static double top_P;   /* Minimum allowed depth */
short int stage;   /* Master mode control variable */
static double telem_Step;   /* Commanded change in target */
static double Home_days;    /* Home about this often */
static short Commskipmax; /* maximum number of comms that can be skipped by timers - a safety feature */
static double MaxQuietDays; /*maximum number of days without a comm mode - ERROR - another safety feature */
static double sleepduration;// Total sleep time sec


/**************************************************************************************************
        Declare global mission-specific parameters - Tier II
   Mission is so simple that these are pretty simple also
****************************************************************************************************/

/* Initialize parameter values and link them with the Ptable */

static struct mission { // Parameters and structures to move float up to surface
    struct servo_p Servo_P1, Servo_P2;
    struct up Up;
    struct up EmergencyUp;
    struct bocha Bocha;
    struct bocha EmergencyBocha;

} Mission;

/* structure to keep track of ice detection logic*/
// number of ice-condition "ecpochs"
// each ends on a particular day and has its own rules
// last one has just one rule: attempt to surface no matter what

# define N_ICE_EPOCH (3)
static struct ice
{
	long IntervalSec; // burst interval
	long DurationSec; // burst duration
	long DelaySec;		// delay before the first burst (after start of a mode, check against mode_time)
	double PatmUsed;			// Patm used by the altimeter, must match surfcheck.c!
	double Patm;			// Best guess of the actual Patm
	double IceReturnScale;	// Scaling used to return ice draft from surfcheck  (must match surfcheck.c!)

    double MaxPhotoP; // max depth to take photos

	double EpochYearDay[N_ICE_EPOCH]; // ending dates of each epoch;

	short NeedGreenCount[N_ICE_EPOCH]; // min. number of "green flags" to surface?
	short IgnoreBad[N_ICE_EPOCH]; // Ignore bad returns (otherwise, each one resets the GreenCount)
	short SurfaceEarly[N_ICE_EPOCH]; // whether to surface as soon as the surfacing rule is satisfied (otherwise let the mode time out)
	short act_now; // Flag set by the timer, indicates that the decision will be made based on the next run
	short okToSurface; // Latest verdict (remember to reset when in doubt!)
    short SurfcheckRequested; // Indicates that surfcheck was requested during the sampling cycle (1: altimeter only, 2: with photo. *Only used in simulator*)

	// other options...
//	double IceMax[N_ICE_EPOCH]; // max. ice thickness to count as "green flag" (in conjunction with open-water flag or ice flag? 
//	double MinCommInterval[N_ICE_EPOCH]; // min. interval since the last comm

	short Run; // enable altimeter bursts (set in next_mode, hard-wired)
	double day_time_last; // save day_time of the previous burst
	int Count; // number of results since last reset (for information)
	int GreenCount; // number of consecutive "green" flags (reset at the start of the mode)

    int PhotosTaken; // # of photos taken in a current series
    short NeedPhoto; // indicates that a photo should be taken on the next run
} Ice;


void initialize_mission_parameters(void){
    int i;
    /*******************************************/
    /* Hack to set the ice epochs relative to the start time during the test mission  */
    time_t now;
    struct tm  *tm;
    double yday;
    // Get real time:
    time(&now);
    tm = gmtime(&now);
    yday = (double)tm->tm_yday + (double)tm->tm_hour / 24.0 + (double)tm->tm_min / 24.0 / 60.0 + (double)tm->tm_sec / 24.0 / 60.0 / 60.0;

    /*******************************************/
    /* Sampling intervals
    ** Note that for the profiles, the "sampling interval" is now
    ** used to specify the maximum time allowed for ballast adjustment
    ** during each sample.
    */
    Si[MODE_PROFILE_UP] = 30;
    Si[MODE_PROFILE_DOWN] = 30;
    Si[MODE_PROFILE_SURF] = 30;


    Si[MODE_SETTLE] = 30;
	Si[MODE_SETTLE_P] = 50;
    Si[MODE_DRIFT_ISO] = 30;
    Si[MODE_DRIFT_ML] = 30;
    Si[MODE_DRIFT_SEEK] = 30;
    Si[MODE_SERVO_P] = 50;
    Si[MODE_BOCHA] = 50;

	/************************************************************************************************
	Initialization of global (external) defaults
	*************************************************************************************************/
	set_param_int("down_home", 1);	// Always home at the end of DOWN
	set_param_int("aux:xfer_tmax", 600); // AUX file transfer timeout
    
    /************************************************************************************************
    Initialization of local static defaults
    ************************************************************************************************/
    Mass0 = 50.00;  /* initial mass */
    Creep = 0.00e-6;  /* kg/day */
    deep_control = 25.e-6;
    shallow_control = 50.e-6;  /*  m^2/dbar */
    bottom_P = 150.;  /* Depth to push out piston */
    error_P = 150.; /* Depth to declare emergency */
    top_P = -100;   /* Minimum allowed depth */
    stage = 0;   /* Master mode control variable */
    telem_Step = 0.1;   /* Commanded change in target */
    Home_days = 1;    /* Home about this often */
    Commskipmax = 3; /* maximum number of comms that can be skipped by timers - a safety feature */
    MaxQuietDays = 1000; /*maximum number of days without a comm mode - ERROR - another safety feature */
    sleepduration = 8000;// Total sleep time sec

    Error.Modes = -1;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    Error.Pmin = -1000;      /* Minimum Pressure  allowed */
    Error.Pmax = 1000;     /* Maximum Pressure  allowed */
    Error.timeout = 3600;    /* time outside of this range before error is declared*/

    /************************************************************************************************
         Initialization of Tier 1 (default) mode parameters.
    *************************************************************************************************/

    Down.Pmax = 80; /* Max depth of down-leg */
    Down.timeout = 4000;   /* 4000 Max time of down-leg  */
    Down.Sigmax = 5000;     /* Maximum potential density - ends mode */
    Down.Bmin = 0;              /* m^3  Bocha setting  */
    Down.Speed = 0.11;      /* 0.11 target speed */
    Down.Brate = 0.;      /* 5e-8 Bocha speed */
    Down.Pspeed = 2.;   /* don't control speed shallower than this */
    Down.drogue = 0;      /* drogue position: 0 closed, 1 open */

    Up.Bmax = 650.e-6;    /* max bocha setting*/
    Up.Speed = 0.07;   /* 0.07 target speed */
    Up.Brate = 0.05e-6;   /* bocha rate to use for speed control */
    Up.Pend = 3;    /* Minimum Pressure - end of up-leg */
    Up.timeout = 900; /* Max duration of up-leg   */
    Up.Speedbrake = 1; /* if 1, use drogue as speedbrake until float rises */
    Up.PHyst = 3.;     /* Hysterisis (m) allowed in speed control */
    Up.drogue = 0;    /* 0 or 1 - drogue open or closed during up */
    Up.surfacetime= 180.;  /* time/sec past reaching up.Pend before end */
    
    Servo_P.Pmin=5;   /* end if it goes above this depth */
    Servo_P.Pmax=100; /* end if it goes below this depth */
    Servo_P.Pgoal=6;  /* push out Bocha if shallower */
    Servo_P.Bspeed=0.1e-6; /* bocha increase speed when below Pgoal, m^3/s */
    Servo_P.BspeedEtime=1.e7; /* efolding time to decrease Bspeed (sec) */
    Servo_P.BspeedShallow=2.e-7;  /* bocha reduction speed when above Pgoal, m^3/s*/
    Servo_P.timeout=2000;  /* timeout seconds */
    Servo_P.drogue=0;      /* drogue position: 0 closed, 1 open */

    Settle.secOday = -1;   /* end Settle when time passes this clock time [0 86400] (obsolete: replaced by timers) */
    Settle.timeout = 7200;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    Settle.seek_time = 0.8;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    Settle.decay_time = 0.4;      /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    Settle.nav = 20;                /* number of points to average to get settled volume */
    Settle.drogue_time = 0; /* time after start of settle to keep drogue open */
    Settle.beta = 0; /* pseudo compressive gain (big for stable)*/
    Settle.tau = 400;             /* seek gain (big for stable)*/
    Settle.weight_error = 1;    /* Accept seek values if Dsig*V0 is     less than this (in kg)*/
    Settle.Vol_error = 0.5e-6;       /* 0.5e-6 Accept volume if stdev of V0 is less     than this ( if <0 no volume set) */
    Settle.Ptarget = 15;      /* Target Pressure - used only with Nfake */
    Settle.Nfake = 0;                   /* Fake stratification relative to Ptarget  No effect if 0 */
    Settle.nskip = 1;           /*      (if 0, skip settle mode always)  */
    Settle.SetTarget = 0; /* How to set target:         1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    Settle.Target = 1022.4;          /* Settle Target isopycnal */
    Settle.B0 = 50e-6;              /* bocha,  don't need to set */
    Settle.Bmin = 150.e-6; /* Minimum Bocha */
    Settle.Bmax = 150.e-6; /* Maximum Bocha */

	
	Settle_P.Ptarget = 80;   /* target depth /m */
	Settle_P.Pmin = -1000;	/* hard band max*/
	Settle_P.Pmax = 1000;	/* hard band max*/
	Settle_P.HardBrate = 25.e-6 / Si[MODE_SETTLE_P];		/* Hard-band control rate, m^3/dbar/sec, similar to deep_control*dt */
	Settle_P.SetB0 = 0; /* How to set B0: 1-current B, 2-B corresponding to Ballast.target density, else constant (set B0 below) */
	Settle_P.B0 = 90.e-6;		 /* first guess of ballast position at Ptarget */ //<< not used
	Settle_P.Kp = 5.e-9;   /* Proportional control [m^3/sec / dbar], correspond to Safe.dbdp */
	Settle_P.Ki = 0;// 1.e-10;   /* Integral control [m^3/sec^2 / dbar], correspond to Safe.Alpha*/
	Settle_P.timeout = 10;// 7200;  /* Max duration */
	Settle_P.drogue = 0;    /* 0 or 1 - drogue open or closed  */
	
    Bocha.B = 150.e-6; /* Constant bocha to set*/
    Bocha.Btol = 0.5e-6; /* Tolerance of Bocha */
    Bocha.timeout = 600; /* Max duration */
    Bocha.drogue = 0;    /* 0 or 1 - drogue open or closed  */

    Drift.SetTarget = 3; /*  How to set Target:     1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    Drift.VoffZero = 1;  /* 1 to set Voff=0 at drift start, else keep value */
    Drift.median = 1;  /* 1 use 5 point median filter, 0 don't */
    Drift.timetype = 2; /* How to end Drift mode 1: Use time since end of last drift mode or mission start 2: At the given timeOday (defaults to noon = 0.5)    Also use timeout */
    Drift.time_end_sec = -1;   /*time/seconds to end Drift mode */
    Drift.timeout_sec = 500.;      /* Additional timeout to end mode / seconds*/
    Drift.Tref = 8;      /* reference temperature */
    Drift.Voff = 0;       /* Ballast adjustment during Drift  */
    Drift.Voffmin = -200e-6;  /*prevent negative runaway on bottom */
    Drift.Moff = 0;      /* Offset of Mass from Mass */
    Drift.Air = 15.5e-6;      /* grams of air buoyancy at surface -always used*/
    Drift.Compress = 3.1e-6;  /* float compressibility db^-1*/
    Drift.Thermal_exp = 0.722e-4;     /* float thermal expansion coeff */
    Drift.Target = 1024.0;         /* target isopycnal */
    Drift.iso_time = 7200;        /*seek time toward surface  sec  */
    Drift.seek_Pmin = -100;      /* depth range for drift seek mode*/
    Drift.seek_Pmax = -50;      /* continued */
    Drift.iso_Gamma = 1;/*Pseudo-compressibility  m^3/unit, 1 = isopycnal*/
    Drift.time2 = 600;  /* second timeout parameter -- compatibility only, DO NOT USE  */
    Drift.closed_time = 200;   /* drogue opens after this time   sec  */

    Ballast.SetTarget = 1;  /* how to set Ballast.Target    1,2,3-no changes,    4- end of good Settle      5 - end of Drift, 6-end of Down, 7-end of Up,   8- Value at P=Ballast.Pgoal     9- Value from GetMLB (not implemented yet) */
    Ballast.Vset = 0;    /*set V0: 0 - fixed/manual; 1 - from good settles,     2 - from Settle and Drift end, 3 from drift end only */
    Ballast.MLsigmafilt = 0;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    Ballast.MLthreshold = -100;   /* Switch to ML ballasting if P<Pdev*[]     */
    Ballast.MLmin = 2;            /*        or if P<     */
    Ballast.MLtop = 2;          /* or between top and bottom */
    Ballast.MLbottom =10;
    Ballast.SEEKthreshold = -100;   /* Allow isoSEEK if P>Pdev*[]  & Pressure between Drift.seek_* limits   */
    Ballast.Offset = 0;         /* adds offset to bocha in drift mode */
    Ballast.T0 = 26.8;      /* most recent temperature at ballasted point */
    Ballast.S0 = 37.45;      /* most recent Salinity  */
    Ballast.P0 = 0;      /* most recent Pressure*/
    Ballast.rho0 = 1024.628;    /* most recent water (=float) density (used to be important)*/
    Ballast.B0 = 160.e-6;      /* most recent bocha setting at ballasted point */
    Ballast.V0 = 48700.00e-6;      /* Estimated float volume at zero bocha, at surface, ref Temp, no air -- IMPORTANT - First guess for Settle*/
    Ballast.TH0 = 26.8;     /* most recent potential temperature */
    Ballast.Vdev = 0;  /* most recent stdev of float volume estimates */
    Ballast.Pgoal = 30;  /* if STP==2, set STP at this depth */
    Ballast.Target = 1022.4; /* target isopycnal e.g. 1022 */

    CTD.which = BOTTOMCTD;  /* How to get one CTD value from two CTD's - BOTTOMCTD ,TOPCTD, MEANCTD or MAXCTD */
    CTD.BadMax = -1;    /*How many bad CTDs before error   Long integer - Can be up to 2.1e9; -1 to disable*/
    CTD.Ptopmin = 2.1;      /* TopCTD not good above this pressure */
    CTD.Poffset = 0.106;      /* Offset for Pressure to be zero when top of float is at surface, adjusts pressure to be that at middle of float hull*/
    CTD.Separation = 1.42;  /* Distance between two CTDs*/
    CTD.TopSoffset = 0;    /* Correction to top CTD salinity */
    CTD.TopToffset = 0;   /*Correction to top CTD temperature*/
    CTD.BottomSoffset = 0;   /* Correction to Bottom CTD salinity */
    CTD.BottomToffset = 0;   /*Correction to Bottom CTD temperature*/

    Mlb.go = 1;  /* 1 to compute MLB target, otherwise don't */
    Mlb.record = 0;   /* 1 to record data in arrays; else don't */
    Mlb.point = 0;       /* points at next open element in raw arrays, 0-> empty */
    Mlb.Nmin = 45;    /* minimum number of data to do computation */
    Mlb.dP = 2;     /* grid spacing m */
    Mlb.dSig = 0.02;  /* bin size for Sigma search */
    Mlb.Sigoff = 0.2; /* Offset of goal from ML density, final target = MLsigma + Sigoff */
    //Mlb.Psave = 0; /* array of raw pressure */
    //Mlb.Sigsave =0 ; /* array of raw potential density */
    //Mlb.Pgrid = 0; /* array of gridded pressure */
    //Mlb.Siggrid = 0; /* array of gridded potential density */


    Bugs.start = -1;  /* sunset  / seconds of GMT day 0-86400 */
    Bugs.stop = -1;  /* sunrise */
    Bugs.start_weight = 0;  /* sunset weight / kg  */
    Bugs.stop_weight = 0;  /* sunrise weight /kg - linear interpolation between */
    Bugs.flap_interval = 10000;  /* time between flaps / seconds */
    Bugs.flap_duration = 130;  /* time between close and open */
    Bugs.weight = 0; /* weight - don't set  */


	
    Timer.enable = 0;       // ==1 if all timers are enabled (individual timers can be controlled by setting the time ore stage to -1
	Timer.time1 = 11L * 60 * 60; //  75600;  // time (in seconds since midnight GMT) for the first timer event. May want to change to multy-day timers eventually
	Timer.time2 = 23L * 60 * 60;;       // ... for the second timer event
    Timer.time3 = -1;       // ... for the third timer event
    Timer.time4 = -1;       // ... for the fourth timer event
    Timer.command1 = 1;       // timer command to issue when the first timer is triggered (set to -1 to disable)
	Timer.command2 = 1;   // ... second timer
	Timer.command3 = 0;   // ... third timer
	Timer.command4 = 0;   // ... fourth timer
    Timer.nskip1 = 0;   // How many timer triggers to skip?
    Timer.nskip2 = 0;   // timer2
    Timer.nskip3 = 0;   // timer3
    Timer.nskip4 = 0;   // timer4

    Timer.countdown1=Timer.nskip1; // DON'T SET - internal countdown variable
    Timer.countdown2=Timer.nskip2; // DON'T SET - internal countdown variable
    Timer.countdown3=Timer.nskip3; // DON'T SET - internal countdown variable
    Timer.countdown4=Timer.nskip4; // DON'T SET - internal countdown variable
    
/************************************************************************************************
Initialization of Tier 2 parameters.
These determine variations of the Mode parameters above for each particular stage
*************************************************************************************************/

 //   Mission.Pskim=6.;  // Target depth for float - below safedepth
 //   Mission.Pslow=40.; // Start creeping up slowly from this depth upward

	Mission.Servo_P1=Servo_P;  // Initial Mission up
	Mission.Servo_P1.Pmin = 40;
    Mission.Servo_P1.Pmax=1000; 
    Mission.Servo_P1.Pgoal = -100;
    Mission.Servo_P1.Bspeed=0.05e-6;
    Mission.Servo_P1.BspeedShallow = -Mission.Servo_P1.Bspeed;
    Mission.Servo_P1.timeout=4000.;

    Mission.Servo_P2=Servo_P;  // Surfacing very slow, try to settle at ~5-10 m.
	Mission.Servo_P2.Pmin = -100;
    Mission.Servo_P2.Pmax=	80;  
	Mission.Servo_P2.Pgoal = 6;
    Mission.Servo_P2.Bspeed=0.007e-6;
    Mission.Servo_P2.BspeedEtime=20000; /* efolding time to increase Bspeed (sec) */
    Mission.Servo_P2.BspeedShallow = 2*Mission.Servo_P2.Bspeed;
    Mission.Servo_P2.timeout = 21600.;
    
    Mission.Up = Up; // regular UP, used before surfacing
    Mission.EmergencyUp = Up; // emergency UP, in response to over-pressure error
    Mission.EmergencyUp.Pend = 30; 
    Mission.EmergencyUp.timeout = 4000; /* Max duration of up-leg   */
    Mission.EmergencyUp.Brate = 0; 
    Mission.EmergencyUp.surfacetime = 0.;  /* time/sec past reaching up.Pend before end */

    
    Mission.Bocha = Bocha;
    Mission.EmergencyBocha = Bocha;
    Mission.EmergencyBocha.timeout = 4000;
    Mission.EmergencyBocha.Btol = -1; // force to run till timeout


	Ice.IntervalSec = 3600; // burst interval
	Ice.DurationSec = 500; // burst duration
	Ice.DelaySec = 3600;		// delay before the first burst (after start of a mode, check against mode_time)
    Ice.PatmUsed = 9.90;			// Patm used by the altimeter, must match surfcheck.c!
	Ice.Patm = 10.0;			// Best guess of the actual Patm
	Ice.IceReturnScale = 10.0;  // Scaling used to return ice draft from surfcheck  (must match surfcheck.c!)
    Ice.SurfcheckRequested = 0; // Set internally to signal that surfcheck has been requested and we should expect surfcheck result

    Ice.MaxPhotoP = 16.5; // max depth to take photos

#ifdef SIMULATION
	Ice.EpochYearDay[0] = yday + 0.5;// 243;//  1 Sep. 
	Ice.EpochYearDay[1] = yday + 1;//250;//  8 Sep. 
	Ice.EpochYearDay[2] = yday + 1.5;//257;// 15 Sep.
#else
    Ice.EpochYearDay[0] = 243;//  1 Sep. 
    Ice.EpochYearDay[1] = 250;//  8 Sep. 
    Ice.EpochYearDay[2] = 257;// 15 Sep.
#endif

	Ice.IgnoreBad[0] = 0; // Ignore bad returns? (otherwise, each one resets the GreenCount)
	Ice.IgnoreBad[1] = 0;
	Ice.IgnoreBad[2] = 0;

	Ice.NeedGreenCount[0] = 2; // min. number of "green flags" to surface?
	Ice.NeedGreenCount[1] = 1;
	Ice.NeedGreenCount[2] = 0;

	Ice.SurfaceEarly[0] = 0; // whether to surface as soon as the surfacing rule is satisfied (otherwise let the mode time out)
	Ice.SurfaceEarly[1] = 0;
	Ice.SurfaceEarly[2] = 0;


    /* Add mission-specific Ptble entries */
    /* These macros expand to add_param("Mission.Servo_P0.Pmin",	PTYPE_DOUBLE, &Mission.Servo_P0.Pmin), etc. */
    /* Use ADD_PTABLE_D for PTYPE_DOUBLE, ADD_PTABLE_L for PTYPE_LONG, and ADD_PTABLE_S for PTYPE_SHORT.*/

    ADD_PTABLE_D(   Mission.Servo_P1.Pmin  );
    ADD_PTABLE_D(   Mission.Servo_P1.Pmax  );
    ADD_PTABLE_D(   Mission.Servo_P1.Pgoal  );
    ADD_PTABLE_D(   Mission.Servo_P1.Bspeed  );
    ADD_PTABLE_D(   Mission.Servo_P1.BspeedEtime  );
    ADD_PTABLE_D(   Mission.Servo_P1.BspeedShallow  );
    ADD_PTABLE_D(   Mission.Servo_P1.timeout  );
    ADD_PTABLE_S(   Mission.Servo_P1.drogue);
    
    ADD_PTABLE_D(   Mission.Servo_P2.Pmin   );
    ADD_PTABLE_D(   Mission.Servo_P2.Pmax   );
    ADD_PTABLE_D(   Mission.Servo_P2.Pgoal  );
    ADD_PTABLE_D(   Mission.Servo_P2.Bspeed );
    ADD_PTABLE_D(   Mission.Servo_P2.BspeedEtime    );
    ADD_PTABLE_D(   Mission.Servo_P2.BspeedShallow  );
    ADD_PTABLE_D(   Mission.Servo_P2.timeout    );
    ADD_PTABLE_S(   Mission.Servo_P2.drogue );
    
    ADD_PTABLE_D(   Mission.Bocha.B );
    ADD_PTABLE_D(   Mission.Bocha.Btol  );
    ADD_PTABLE_D(   Mission.Bocha.timeout   );
    ADD_PTABLE_S(   Mission.Bocha.drogue    );

    ADD_PTABLE_D(   Mission.EmergencyBocha.B    );
    ADD_PTABLE_D(   Mission.EmergencyBocha.Btol );
    ADD_PTABLE_D(   Mission.EmergencyBocha.timeout  );
    ADD_PTABLE_S(   Mission.EmergencyBocha.drogue   );

    ADD_PTABLE_L(   Ice.IntervalSec  );
    ADD_PTABLE_L(   Ice.DurationSec  );
    ADD_PTABLE_L(   Ice.DelaySec  );
    ADD_PTABLE_D(   Ice.PatmUsed  );
    ADD_PTABLE_D(   Ice.Patm  );
    ADD_PTABLE_D(   Ice.IceReturnScale  );
    ADD_PTABLE_S(   Ice.SurfcheckRequested  );

    ADD_PTABLE_D(   Ice.MaxPhotoP  );

    ADD_PTABLE_D(   Ice.EpochYearDay[0]  );
    ADD_PTABLE_D(   Ice.EpochYearDay[1]  );
    ADD_PTABLE_D(   Ice.EpochYearDay[2]  );

    ADD_PTABLE_S(   Ice.IgnoreBad[0]  );
    ADD_PTABLE_S(   Ice.IgnoreBad[1]  );
    ADD_PTABLE_S(   Ice.IgnoreBad[2]  );

    ADD_PTABLE_S(   Ice.NeedGreenCount[0]  );
    ADD_PTABLE_S(   Ice.NeedGreenCount[1]  );
    ADD_PTABLE_S(   Ice.NeedGreenCount[2]  );

    ADD_PTABLE_S(   Ice.SurfaceEarly[0]  );
    ADD_PTABLE_S(   Ice.SurfaceEarly[1]  );
    ADD_PTABLE_S(   Ice.SurfaceEarly[2]  );
};
