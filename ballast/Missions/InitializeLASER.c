/* This file is included in ballast.c */
/* It sets initial values of all control variables, so that the main code
   does not need to be modified when values change */
/* Do not change this variable */
static const char *__id__ = "$Id$";

/* Oct 2014 - ORCA - Massive simplification of this code since mission is so simple
   No Level II parameters */
// 25 July 2012 - separate parameter declaration (global) and initialization(in setup() function), in anticipation of the two-tier parameter model
// Nov 2015 - Rebuilt for LASER from MIZ and BENGAL - some MIZ functionality is archived
// Nov 2015 - Using new macros to add parameters to Ptable in a uniform and streamlined manner (AS)



# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN  900  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 65 /* Number of real commands  1 2 3 ... NCOMMAND */
# define LOGDAYSSIM 10  /* Stop ballastlog after this many days in simulation */

/* Declare misc. local scalar parameters - all satellite settable.  Actual initialization is in initialize_mission_parameters()*/
static double Mass0 ;  /* initial mass */
static double Creep;  /* kg/day */
static double deep_control;
static double shallow_control;  /*  m^2/dbar */
static double bottom_P;  /* Depth to push out piston */
static double error_P; /* Depth to declare emergency */
static double top_P;   /* Minimum allowed depth */
short int stage;   /* Master mode control variable */
static double telem_Step;   /* Commanded change in target */
static double Home_days;    /* Home about this often */
static short Commskipmax; /* maximum number of comms that can be skipped by timers - a safety feature */
static double MaxQuietDays; /*maximum number of days without a comm mode - ERROR - another safety feature */
static double sleepduration;// Total sleep time sec


/**************************************************************************************************
        Declare global mission-specific parameters - Tier II
   Mission is so simple that these are pretty simple also
****************************************************************************************************/

/* Initialize parameter values and link them with the Ptable */

static struct psballast {
	// This is an "old style" structure; a "new" version should look like this:
	//	struct down Down[2];
	//	struct settle Settle[2];
	//	struct drift Drift[2];
	//	struct up Up;
	// -AS
    double depth1;	// Depth of the first (captive) dive & drift
    double settle1_time;	// length of the first (captive) settle - typically, very short (was: Steps.time0)
    double drift1_time;	// length of the first (captive) drift - typically, very short (was: Drift.time2)
    
    double depth2;	// Depth of the second (longer) dive & drift
    double settle2_time;	// length of the second (longer) settle (was: Steps.time1)
    double settle2_seektime;	// length of seeking time during settle
    double drift2_time;	// length of the second (longer) drift. NB: Vset==1 for this drift! (was: Steps.time5)
} PSBallast;



static struct steps{
    // consists of up-comm-down-4xSettle cycle:
    struct up Up;
    struct down Down;
    // and 4 settles (which can be different!)
    struct settle Settle[4];
    //double z[4];       // Settle depths,  mapped to Steps.z1-4 in Ptable
    //double time[4];	   // Settle durations, mapped to Steps.time1-4 in Ptable
    // double decay_ratio;  // Not needed here - set appropriate (fractional) Settle.decay_time /* usually settle.decay_time=steps.decay_ratio*settle.seek_time */
} Steps;

static struct turb{
    // consists of up-comm-down-Up3-drift cycle:
    struct up Up;
    struct down Down;
    // another up (insertion):
    struct up Up3;
    // No Drift or Ballast for now - its too complex
} Turb;

static struct profile{
    // consists of up-comm-down cycle:
    struct up Up;
    struct down Down;
    short ncycles;  // number of cycles before COMM
} Profile;

static struct subduct{
    // Fixed Bocha <--> Comm until dive is set, then down -> settle -> Up -> COMM
    struct up Up;
    struct bocha Bocha;
    struct settle Settle;
    struct down Down;
    short dive;  // 0 for bocha, 1 for settle, set by hand
    double Sigoff;  // Target density offset from current
} Subduct;

void initialize_mission_parameters(void){
    
    int k;

    /*******************************************/
    /* Sampling intervals
    ** Note that for the profiles, the "sampling interval" is now
    ** used to specify the maximum time allowed for ballast adjustment
    ** during each sample.
    */
    Si[MODE_PROFILE_UP] = 30;
    Si[MODE_PROFILE_DOWN] = 30;
    Si[MODE_PROFILE_SURF] = 30;


    Si[MODE_SETTLE] = 30;
	Si[MODE_SETTLE_P] = 30;
    Si[MODE_DRIFT_ISO] = 30;
    Si[MODE_DRIFT_ML] = 30;
    Si[MODE_DRIFT_SEEK] = 30;
    Si[MODE_SERVO_P] = 30;
    Si[MODE_BOCHA] = 30;

	/************************************************************************************************
	Initialization of global (external) defaults
	*************************************************************************************************/
	set_param_int("down_home", 1);	// Always home at the end of DOWN
	// set_param_int("aux:xfer_tmax", 600); // (MIZ) AUX file transfer timeout
    
    /************************************************************************************************
    Initialization of local static defaults
    ************************************************************************************************/
    Mass0 = 50.00;  /* initial mass */
    Creep = 0.00e-6;  /* kg/day */
    deep_control = 25.e-6;
    shallow_control = 50.e-6;  /*  m^2/dbar */
    bottom_P = 150.;  /* Depth to push out piston */
    error_P = 220.; /* Depth to declare emergency */
    top_P = -100;   /* Minimum allowed depth */
    stage = 0;   /* Master mode control variable */
    telem_Step = 0.1;   /* Commanded change in target */
    Home_days = 1;    /* Home about this often */
    Commskipmax = 3; /* maximum number of comms that can be skipped by timers - a safety feature */
    MaxQuietDays = 2; /*maximum number of days without a comm mode - ERROR - another safety feature */
    sleepduration = 8000;// Total sleep time sec

    Error.Modes = -1;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    Error.Pmin = -1000;      /* Minimum Pressure  allowed */
    Error.Pmax = 1000;     /* Maximum Pressure  allowed */
    Error.timeout = 3600;    /* time outside of this range before error is declared*/

    /************************************************************************************************
         Initialization of Tier 1 (default) mode parameters.
    *************************************************************************************************/

    Down.Pmax = 80; /* Max depth of down-leg */
    Down.timeout = 4000;   /* 4000 Max time of down-leg  */
    Down.Sigmax = 5000;     /* Maximum potential density - ends mode */
    Down.Bmin = 0;              /* m^3  Bocha setting  */
    Down.Speed = 0.11;      /* 0.11 target speed */
    Down.Brate = 0.;      /* 5e-8 Bocha speed */
    Down.Pspeed = 2.;   /* don't control speed shallower than this */
    Down.drogue = 0;      /* drogue position: 0 closed, 1 open */

    Up.Bmax = 650.e-6;    /* max bocha setting*/
    Up.Speed = 0.07;   /* 0.07 target speed */
    Up.Brate = 0.05e-6;   /* bocha rate to use for speed control */
    Up.Pend = 3;    /* Minimum Pressure - end of up-leg */
    Up.timeout = 900; /* Max duration of up-leg   */
    Up.Speedbrake = 1; /* if 1, use drogue as speedbrake until float rises */
    Up.PHyst = 3.;     /* Hysterisis (m) allowed in speed control */
    Up.drogue = 0;    /* 0 or 1 - drogue open or closed during up */
    Up.surfacetime= 180.;  /* time/sec past reaching up.Pend before end */

	UpSurf.Bmax = 650.e-6;    /* max bocha setting*/
	UpSurf.Speed = 0.05;   /* target speed */
	UpSurf.Brate = 0;   /* bocha rate to use for speed control 0 = as fast as possible*/
	UpSurf.Pend = 3;    /* Minimum Pressure - end of up-leg */
	UpSurf.timeout = 3600; /* Max duration of up-leg   */
	UpSurf.Speedbrake = 1; /* if 1, use drogue as speedbrake until float rises */
	UpSurf.PHyst = 10;     /* Hysterisis (m) allowed in speed control */
	UpSurf.drogue = 1;    /* 0 or 1 - drogue open or closed during up */
	UpSurf.surfacetime = 120;  /* time/sec past reacing up.Pend before end */
	UpSurf.overlaptime = 120;  /* time/sec during which SSAL and upper CTD are run together (via SENS_SSAL_CAL) */

    
    Servo_P.Pmin=5;   /* end if it goes above this depth */
    Servo_P.Pmax=100; /* end if it goes below this depth */
    Servo_P.Pgoal=6;  /* pull in Bocha if shallower than this*/
    Servo_P.Bspeed=0.1e-6; /* bocha increase speed when below Pgoal, m^3/s */
    Servo_P.BspeedEtime=1.e7; /* efolding time to decrease Bspeed (sec) */
    Servo_P.BspeedShallow=2.e-7;  /* bocha reduction speed when above Pgoal, m^3/s*/
    Servo_P.timeout=2000;  /* timeout seconds */
    Servo_P.drogue=0;      /* drogue position: 0 closed, 1 open */

    Settle.secOday = -1;   /* end Settle when time passes this clock time [0 86400] (obsolete: replaced by timers) */
    Settle.timeout = 7200;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    Settle.seek_time = 0.8;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    Settle.decay_time = 0.4;      /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    Settle.nav = 20;                /* number of points to average to get settled volume */
    Settle.drogue_time = 300.; /* time after start of settle to keep drogue open */
    Settle.beta = 2; /* pseudo compressive gain (big for stable)*/
    Settle.tau = 400;             /* seek gain (big for stable)*/
    Settle.weight_error = 1;    /* Accept seek values if Dsig*V0 is     less than this (in kg)*/
    Settle.Vol_error = 0.5e-6;       /* 0.5e-6 Accept volume if stdev of V0 is less     than this ( if <0 no volume set) */
    Settle.Ptarget = 15;      /* Target Pressure - used only with Nfake */
    Settle.Nfake = 0;                   /* Fake stratification relative to Ptarget  No effect if 0 */
    Settle.nskip = 1;           /*      (if 0, skip settle mode always)  */
    Settle.SetTarget = 0; /* How to set target:  1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    Settle.Target = 1022.4;          /* Settle Target isopycnal */
    Settle.B0 = 50e-6;              /* bocha,  don't need to set */
    Settle.Bmin = 0.e-6; /* Minimum Bocha */
    Settle.Bmax = 650.e-6; /* Maximum Bocha */

	/* --------  NOTE SETTLE_P IS EXPERIMENTAL   --------  */
	Settle_P.Ptarget = 80;   /* target depth /m */
	Settle_P.Pmin = -1000;	/* hard band max*/
	Settle_P.Pmax = 1000;	/* hard band max*/
	Settle_P.HardBrate = 25.e-6 / Si[MODE_SETTLE_P];		/* Hard-band control rate, m^3/dbar/sec, similar to deep_control*dt */
	Settle_P.SetB0 = 0; /* How to set B0: 1-current B, 2-B corresponding to Ballast.target density, else constant (set B0 below) */
	Settle_P.B0 = 90.e-6;		 /* first guess of ballast position at Ptarget */ //<< not used
	Settle_P.Kp = 5.e-9;   /* Proportional control [m^3/sec / dbar], correspond to Safe.dbdp */
	Settle_P.Ki = 0;// 1.e-10;   /* Integral control [m^3/sec^2 / dbar], correspond to Safe.Alpha*/
	Settle_P.timeout = 10;// 7200;  /* Max duration */
	Settle_P.drogue = 0;    /* 0 or 1 - drogue open or closed  */
	
    Bocha.B = 150.e-6; /* Constant bocha to set*/
    Bocha.Btol = -1; /* Tolerance of Bocha. Set to -1 to disable exit-on-target-B */
    Bocha.timeout = 600.; /* Max duration sec*/
    Bocha.drogue = 0;    /* 0 or 1 - drogue open or closed  */

    Drift.SetTarget = 3; /*  How to set Target:     1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    Drift.VoffZero = 1;  /* 1 to set Voff=0 at drift start, else keep value */
    Drift.median = 1;  /* 1 use 5 point median filter, 0 don't */
    Drift.timetype = 2; /* How to end Drift mode 1: Use time since end of last drift mode or mission start 2: At the given timeOday (defaults to noon = 0.5) - Obsolete - Use Timers instead */
    Drift.time_end_sec = -1;   /* time/seconds to end Drift mode */
    Drift.timeout_sec = 50000.;  /* Additional timeout to end mode / seconds*/
    Drift.Tref = 8;      /* reference temperature */
    Drift.Voff = 0;       /* Ballast adjustment during Drift  */
    Drift.Voffmin = -200e-6;  /*prevent negative runaway on bottom */
    Drift.Moff = 0;      /* Offset of Mass from Mass */
    Drift.Air = 15.5e-6;      /* grams of air buoyancy at surface -always used*/
    Drift.Compress = 3.1e-6;  /* float compressibility db^-1*/
    Drift.Thermal_exp = 0.722e-4;     /* float thermal expansion coeff */
    Drift.Target = 1024.0;         /* target isopycnal */
    Drift.iso_time = 7200;        /*seek time toward surface  sec  */
    Drift.seek_Pmin = -100;      /* depth range for drift seek mode*/
    Drift.seek_Pmax = -50;      /* continued */
    Drift.iso_Gamma = 1;   /*Pseudo-compressibility  m^3/unit, 1 = isopycnal*/
    Drift.time2 = 600;     /* second timeout parameter -- compatibility only, DO NOT USE  */
    Drift.closed_time = 200;   /* drogue opens after this time   sec  */

    Ballast.SetTarget = 1;  /* how to set Ballast.Target    1,2,3-no changes,    4- end of good Settle      5 - end of Drift, 6-end of Down, 7-end of Up,   8- Value at P=Ballast.Pgoal     9- Value from GetMLB (not implemented yet) */
    Ballast.Vset = 0;    /*set V0: 0 - fixed/manual; 1 - from good settles,     2 - from Settle and Drift end, 3 from drift end only */
    Ballast.MLsigmafilt = 0;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    Ballast.MLthreshold = -100;   /* Switch to ML ballasting if P<Pdev*[]     */
    Ballast.MLmin = 2;            /*        or if P<     */
    Ballast.MLtop = 2;          /* or between top and bottom */
    Ballast.MLbottom =10;
    Ballast.SEEKthreshold = -100;   /* Allow isoSEEK if P>Pdev*[]  & Pressure between Drift.seek_* limits   */
    Ballast.Offset = 0;         /* adds offset to bocha in drift mode */
    Ballast.T0 = 26.8;      /* most recent temperature at ballasted point */
    Ballast.S0 = 37.45;      /* most recent Salinity  */
    Ballast.P0 = 0;      /* most recent Pressure*/
    Ballast.rho0 = 1024.628;    /* most recent water (=float) density (used to be important)*/
    Ballast.B0 = 160.e-6;      /* most recent bocha setting at ballasted point */
    Ballast.V0 = 48600.00e-6;      /* Estimated float volume at zero bocha, at surface, ref Temp, no air -- IMPORTANT - First guess for Settle*/
    Ballast.TH0 = 26.8;     /* most recent potential temperature */
    Ballast.Vdev = 0;  /* most recent stdev of float volume estimates */
    Ballast.Pgoal = 30;  /* if STP==2, set STP at this depth */
    Ballast.Target = 1022.4; /* target isopycnal e.g. 1022 */

    CTD.which = MEANCTD;  /* How to get one CTD value from two CTD's - BOTTOMCTD ,TOPCTD, MEANCTD or MAXCTD */
    CTD.BadMax = -1;    /*How many bad CTDs before error   Long integer - Can be up to 2.1e9; -1 to disable*/
    CTD.Ptopmin = 2.1;      /* TopCTD not good above this pressure */
    CTD.Poffset = 0.106;      /* Offset for Pressure to be zero when top of float is at surface, adjusts pressure to be that at middle of float hull*/
    CTD.Separation = 1.42;  /* Distance between two CTDs*/
    CTD.TopSoffset = 0;    /* Correction to top CTD salinity */
    CTD.TopToffset = 0;   /*Correction to top CTD temperature*/
    CTD.BottomSoffset = 0;   /* Correction to Bottom CTD salinity */
    CTD.BottomToffset = 0;   /*Correction to Bottom CTD temperature*/

    Mlb.go = 1;  /* 1 to compute MLB target, otherwise don't */
    Mlb.record = 0;   /* 1 to record data in arrays; else don't */
    Mlb.point = 0;       /* points at next open element in raw arrays, 0-> empty */
    Mlb.Nmin = 45;    /* minimum number of data to do computation */
    Mlb.dP = 2;     /* grid spacing m */
    Mlb.dSig = 0.02;  /* bin size for Sigma search */
    Mlb.Sigoff = 0.2; /* Offset of goal from ML density, final target = MLsigma + Sigoff */
    //Mlb.Psave = 0; /* array of raw pressure */
    //Mlb.Sigsave =0 ; /* array of raw potential density */
    //Mlb.Pgrid = 0; /* array of gridded pressure */
    //Mlb.Siggrid = 0; /* array of gridded potential density */


    Bugs.start = -1;  /* sunset  / seconds of GMT day 0-86400 */
    Bugs.stop = -1;  /* sunrise */
    Bugs.start_weight = 0;  /* sunset weight / kg  */
    Bugs.stop_weight = 0;  /* sunrise weight /kg - linear interpolation between */
    Bugs.flap_interval = 10000;  /* time between flaps / seconds */
    Bugs.flap_duration = 130;  /* time between close and open */
    Bugs.weight = 0; /* weight - don't set  */

	
    Timer.enable = 1;       // ==1 if all timers are enabled (individual timers can be controlled by setting to -1  - This master is turned on/off in mission code to suppress timers)
    Timer.time[0] = 7200.;  // time (in seconds since midnight GMT) for the first timer event.
    Timer.time[1] = 28800.;       // ... for the second timer event
    Timer.time[2] = 50400;       // ... for the third timer event
    Timer.time[3] = -1;       // ... for the fourth timer event
    Timer.command[0] = 3;    // timer command to issue. Usually, this is next stage
	Timer.command[1] = 2;   // ... second timer
	Timer.command[2] = 4;   // ... third timer
	Timer.command[3] = 4;   // ... fourth timer
    Timer.nskip[0] = 0;   // How many timer triggers to skip? For multiday timing.
    Timer.nskip[1] = 0;   // timer2
    Timer.nskip[2] = 0;   // timer3
    Timer.nskip[3] = 0;   // timer4

	for (k = 0; k < NTIMERS;k++)
		Timer.countdown[k]=Timer.nskip[k]; // DON'T SET - internal countdown variable
    
    
/************************************************************************************************
Initialization of Tier 2 parameters.
These determine variations of the Mode parameters above for each particular stage
*************************************************************************************************/
/* ------------------ PS BALLAST STAGE PARAMETERS --------------  */
	PSBallast.depth1 = 60;	// Depth of the first (captive) dive & drift
    PSBallast.settle1_time = 300;	// length of the first (captive) settle - typically, very short (was: Steps.time0)
    PSBallast.drift1_time = 100;	// length of the first (captive) drift - typically, very short (was: Drift.time2)
    PSBallast.depth2 = 60;	// Depth of the second (longer) dive & drift
    PSBallast.settle2_time = 10000;	// length of the second (longer) settle (was: Steps.time1)
    PSBallast.settle2_seektime = 8000;	// length of seeking time during settle
    PSBallast.drift2_time = 1000;	// length of the second (longer) drift. NB: Vset==1 for this drift! (was: Steps.time5)
    
    
/* ------------------ STEPS STAGE PARAMETERS --------------  */
    Steps.Up = Up;             // Start with the default Up and Down
    Steps.Down = Down;
    
    Steps.Down.Pmax=1.;   // This is automatically increased to max of Steps.Settle[].Ptarget + 1
    
    Steps.Settle[0] = Settle;    // Copy Settle to Steps.Settle
    Steps.Settle[1] = Settle;
    Steps.Settle[2] = Settle;
    Steps.Settle[3] = Settle;
    Steps.Settle[0].drogue_time=0;   // No need to open drogue at start of each step
    Steps.Settle[1].drogue_time=0;
    Steps.Settle[2].drogue_time=0;
    Steps.Settle[3].drogue_time=0;
    
    Steps.Settle[0].Ptarget = 110;       // First settle depth,  mapped to Steps.z1 in Ptable
    Steps.Settle[1].Ptarget = 50;		 // second,  mapped to Steps.z2
    Steps.Settle[2].Ptarget = 40;		 // etc.
    Steps.Settle[3].Ptarget = 30;
    
    Steps.Settle[3].beta=1;               // Shallowest settle has only a weak seeking in expected strong stratification
    Steps.Settle[3].tau=1000;
    
    Steps.Settle[0].timeout=10000;     // Settle times
    Steps.Settle[1].timeout=10000;
    Steps.Settle[2].timeout=10000;
    Steps.Settle[3].timeout=50000;     // This is very long - will be interupted by the timer
    
/* ------------------ TURBULENCE STAGE PARAMETERS --------------  */
    Turb.Up = Up;
    Turb.Down = Down;
    Turb.Up3 =  Up; // Danger! Up3 is just another UP.  - Special Insertion Up
    
    Turb.Down.Pmax = 20; // Insertion depth + about 10m
    Turb.Down.Bmin = 0.e-6;
    
    Turb.Up3.surfacetime = 0;
    Turb.Up3.Speed = 0.01;       // target speed
    Turb.Up3.PHyst = 0;
    Turb.Up3.timeout = 3000.;      //  Adjusted to get float to the right place before ending up
    Turb.Up3.Bmax = 650e-6;
    Turb.Up3.Pend = 15.;            // Insertion depth

    /* ------------------ PROFILE STAGE PARAMETERS --------------  */
    Profile.Up=Up;
    Profile.Down=Down;
    
    Profile.Down.Pmax=40; // Dive to this depth
	Profile.Down.Brate = 0; // Disable speed control
	Profile.Down.Pspeed= -2.; // start control when float is this far above up start.
    Profile.Down.drogue=1;   //
	
    Profile.Up.drogue = 1;    /* 0 or 1 - drogue open or closed during up */
	Profile.Up.Brate = 0; // Disable speed control
	//	Profile.Up.Speed = 0.02;   /* 0.07 target speed */
    //	Profile.Up.Brate = 1.e-6;   /* bocha rate to use for speed control */
	//  Profile.Up.PHyst = 3.;     /* Hysterisis (m) allowed in speed control */
    Profile.Up.timeout = 3000.; /* Max duration of up-leg   */
    Profile.Up.Speedbrake = 1; /* if 1, use drogue as speedbrake until float rises above start */
    Profile.Up.surfacetime= 0.;  /* time/sec past reaching up.Pend before end */
    Profile.Up.Bmax=400.e-6;   /* max Bocha - could be useful */
    
    Profile.ncycles=4;    /* skip this many COMMs on surfacing */

    
/* ------------------ SUBDUCTION STAGE PARAMETERS --------------  */
	Subduct.Up = Up;
	Subduct.Down = Down;
	Subduct.Bocha = Bocha;
    Subduct.Settle = Settle;
    
    Subduct.Bocha.B=400.e-6;  // Float on surface with this value of Bocha
    Subduct.dive=0;          // start on surface, not diving
    Subduct.Down.Pmax=5;     // Insert to 5m before choosing Target
    Subduct.Sigoff=0.25;      // Offset of Settle.Target from Down.Pmax value

    Subduct.Bocha.timeout=3600.; // Comm this often during surface drift
    Subduct.Bocha.Btol= -1;    // If bocha gets closer than this to goal, exit mode
    Subduct.Settle.timeout=100000; // length of subduction
    Subduct.Settle.SetTarget=2;  // Use Ballast.Target to transfer goal to settle mode
    Subduct.Settle.beta=1;      // Isopycnal float
    Subduct.Settle.tau=5000.;  // seek time >> 1/N
    Subduct.Settle.seek_time = 0.99;     // always seek (can't be 1.0 or its not a fraction)
    Subduct.Settle.drogue_time = 4000.;  // Inject with drogue closed
    
    /* Make Ptable links to the Mission-specific (Tier 2) parameters */
    
    /* Add mission-specific Ptble entries */
    /* These macros expand to add_param("PSBallast.depth1",	PTYPE_DOUBLE, &PSBallast.depth1), etc. */
    /* Use ADD_PTABLE_D for PTYPE_DOUBLE, ADD_PTABLE_L for PTYPE_LONG, and ADD_PTABLE_S for PTYPE_SHORT.*/
    
    /* PS Ballast */
	ADD_PTABLE_D(	PSBallast.depth1	);
	ADD_PTABLE_D(	PSBallast.settle1_time	);
	ADD_PTABLE_D(	PSBallast.drift1_time	);
    
	ADD_PTABLE_D(	PSBallast.depth2	);
	ADD_PTABLE_D(	PSBallast.settle2_time	);
	ADD_PTABLE_D(	PSBallast.settle2_seektime	);
	ADD_PTABLE_D(	PSBallast.drift2_time	);
    
	/* Steps (Stage 1) */
	ADD_PTABLE_DOWN(	Steps.Down	);
	ADD_PTABLE_UP(	Steps.Up	);
	ADD_PTABLE_SETTLE(	Steps.Settle[0]	);
	ADD_PTABLE_SETTLE(	Steps.Settle[1]	);
	ADD_PTABLE_SETTLE(	Steps.Settle[2]	);
	ADD_PTABLE_SETTLE(	Steps.Settle[3]	);
    
    /* Turbulence */
	ADD_PTABLE_DOWN(	Turb.Down	);
	ADD_PTABLE_UP(	Turb.Up	);
	ADD_PTABLE_UP(	Turb.Up3	);
    
    /* Profile */
    ADD_PTABLE_DOWN(	Profile.Down	);
    ADD_PTABLE_UP(      Profile.Up      );
    ADD_PTABLE_S(       Profile.ncycles );
    
    /* Subduction */
    ADD_PTABLE_UP(    Subduct.Up	  );
    ADD_PTABLE_DOWN(  Subduct.Down	  );
    ADD_PTABLE_BOCHA( Subduct.Bocha );
    ADD_PTABLE_S(     Subduct.dive  );
    ADD_PTABLE_D(     Subduct.Sigoff );
 

};
