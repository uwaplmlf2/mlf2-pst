/*
 * arch-tag: MLF2 CONTROL SOFTWARE
 * Time-stamp: <2016-09-22 21:29:44 mike>
 *
 * mlf2_ballast.C Ballasting routine for DLF  - with Matlab interface
 * usage:
 * mlf2_ballast(day,P,T,S,T2,S2,B_in,mode_in,drogue_in,daysec,command,B,mode_out,drogue_out,telem_out)
 *
 * Matlab interface:
 *  [ballast_goal,mode_out,drogue_out,telem_out, Ptable] = ballast(day,P,T,S,T2,S2,B_in,mode_in,drogue_in,daysec,command, Ptable)
 *
 * INPUT
 * day - time in days since start of mission
 * daysec - seconds of the current GMT day
 * P - pressure in
 * db (appropriate for chosen CTD combination)
 * T - temperature in deg. C
 * S- salinity in FU
 * T2, S2 - second (upper) CTD
 * command - integer command word
 * B_in - target bocha position in m^3
 * mode_in - mode at input, one of the MODE* constants in ballast.h
 * drogue_in - drogue position (0=closed,1=open)
 *
 * OUTPUT
 * B - ballaster position target in cubic meters
 * mode_out - mode at output
 * drogue_out - drogue position desired
 * telem_out - output telemetry - to trackpoint in Intrusion floats
 *
 * This routine controls mode transitions
 * and bocha motions for all modes
 * but COMM & ERROR
 * It does not move bocha, but returns B to calling routine.
 * It does not move drogue, but returns drogue_out to calling
 * routine
 *
 * Its actions are controlled by variables described in "ballast.h"
 * It does not specify bocha motions during COMM
 * Callingprogram should not call during COMM
 * nor change the value of "mode" until all communications are
 * finished.
 * Values of Mode outside of valid values will trigger an error
 * and return of mode=MODE_ERROR;
 *
 * 7/14/03 - Start CBLAST03 Mods
 * 11/1/03 - Start ADV float mods
 * 2/1/04 - Additional ADV float mods - add Pycnocline mission
 * 2/27/04 - Add EOS mission
 * 6/8/04 - CBLAST04 Mods
 * 7/11/04 - Add GASTEST mission
 * 7/14/04 - Replace with different GASTEST mission
 * 7/28/04- final CBLAST04 changes, add HURRICANEGAS mission, add Creep
 * 8/21/04 - minor mods to PYCNOCLINE mission for equatorial expendable - variables changed
 * 9/06/04 - screening for zero CTD values added - PYCNOCLINE mission only
 * 1/29/05 - Put missions in separate files.  Use Include to put them in if necessary
 * 2/21/05 - South China mods
 * 4/28/06 - AESOP mods - considerable changes
 * 9/21/06 - DOGEE gas float changes
 * 5/30/07 - Intrusion acoustic command floats - extra output variable telemetry_out
 * 11/11/07 - NAB
 * 6/23/08 - Hurricane 2008 - only mission changes
 * 3/19/009 - Hurricane 2009 - added z2sigma routine
 * 11/10/10 - Lake Washington - Add Depth error band - Drift and settle modes only
 * 3/311      - LATMIX
 * 10/06/11   -Lake Washington 2 - add Settle.secOday timeout
 * 5/21/12   - "Global timer" functionality added (AS)
 * 5/21/12   - second "up" added (AS)
 * 7/20/12  - Ptable support in Matlab added. ballast() now returns Ptable
 * 2014 - flags for '-100' bad T/S, Special GTD sampling code
 * 10/12/2014 - Add SLEEP mode support
 *  2/12/2015 - BOCHA Mode added
 *  3/16/2015 - Dedicated functions to handle Timer, Command, and Error exceptions (software interrupts)
 *  3/30/2015 - Rename BOCHA - > SERVO_P mode
 *  3/30/2015 - New BOCHA (B=const) mode
 *	6/06/2016 - *** SPURS2 ***
 *	6/08/2016 - EOS structure
 *  6/09/2016 - Unified UP/DOWN PROFILE mode
 *  6/14/2016 - Old UP/DOWN modes removed!
 *	9/26/2016 - DRAG structure and drag law
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "ballast.h"    /*  This contains function & structure definitions */
#include "mtype.h"      /* Mission-type macros */


void ballast_log(const char *fmt, ...);

#ifndef SIMULATION
# include <stdarg.h>
# include <unistd.h>
# include "util.h"
# include "ptable.h"
# include "config.h"
# include "log.h"

#define RTC() time(NULL)



#else // SIMULATION
/* __________  next section only for Matlab use __________*/
# include <stdarg.h>
#include "mex.h"
#include "ptable_mex.h"
#define log_event printf


/* In Matlab, real-time-clock starts with the simulation */
#define RTC() (day_time*86400.)

/* Input Arguments */

#define DAY_IN          prhs[0]
#define P_IN            prhs[1]
#define T_IN            prhs[2]
#define S_IN            prhs[3]
#define T2_IN           prhs[4]
#define S2_IN            prhs[5]
#define B_IN            prhs[6]
#define M_IN            prhs[7]
#define D_IN            prhs[8]
#define DSEC_IN      prhs[9]
#define CMD_IN       prhs[10]
#define PTABLE_IN       prhs[11]

/* Output Arguments */

#define B_OUT   plhs[0]
#define M_OUT   plhs[1]
#define D_OUT   plhs[2]
#define T_OUT   plhs[3]
#define PTABLE_OUT      plhs[4]

/* mode numbers to pass to the simulator via Ptable */
static short _MODE_PROFILE_DOWN = MODE_PROFILE_DOWN;
static short _MODE_SETTLE = MODE_SETTLE;
static short _MODE_PROFILE_UP = MODE_PROFILE_UP;
static short _MODE_DRIFT_ISO = MODE_DRIFT_ISO;
static short _MODE_DRIFT_SEEK = MODE_DRIFT_SEEK;
static short _MODE_DRIFT_ML = MODE_DRIFT_ML;
static short _MODE_SERVO_P = MODE_SERVO_P;
static short _MODE_BOCHA = MODE_BOCHA;
static short _NR_REAL_MODES = NR_REAL_MODES;
static short _MODE_GPS = MODE_GPS;
static short _MODE_COMM = MODE_COMM;
static short _MODE_ERROR = MODE_ERROR;
static short _MODE_DONE = MODE_DONE;
static short _MODE_START = MODE_START;
static short _MODE_SLEEP = MODE_SLEEP;
static short _MODE_XFER = MODE_XFER;
/* emulation of parameters normally defined elsewhere */
static int down_home = 0;
static short error_code = 0;
static short primary_pr = 0;
#endif /* !SIMULATION */

/* Ballast log format (as a macro for consistency)*/
# define BALLAST_LOG_HEADER "RealTimeClock[s], Mode, Mode_time[s], EOS.Mass[g], EOS.V0[cc], EOS.Air[cc], EOS.Compress, EOS.Thermal_exp, B_Neutral[cc], Target\n"\
							"1,                1,    1,            1e3,         1e6,        1e6,         1e6,          1e6,             1e6,           1\n"  /* scale factors from MKS */

# define BALLAST_LOG_ENTRY ballast_log("%ld,%d,%.0f,%.1f,%.3f,%.1f,%.2f,%.2f,%.2f,%.2f\n",\
	(long)RTC(), mode, mode_time * 86400.,\
	EOS.mass*1000., EOS.V0*1e6, EOS.Air*1e6, EOS.Compress*1e6, EOS.Thermal_exp*1e6,\
	get_bocha(Pressure, Temperature, Density, 0)*1e6, Target-1000.)



/*__________________________________________________________*/
/* Mode switching Routine  next_mode.c
 * Call this at the end of each mode
 * It determines which mode is next and changes parameters as appropriate
 *
 * Different missions have different versions of this subroutine
 * Code is not here, but put in by include at compilation
 */

/* global variables: */
static double save1,save2,save3,STsave[5],ICsave[4],SDsave[13];
static int SDisave[7],isave;
static short Pgoalset=-1; /* 1 if Ballast.Target has been set */
// static short commhome=1;      /* home at end of comm, 1=yes, 2=no */ // not used anymore???
static double PotDensity;            /* global potential density */
static double PressureG;             /* global Pressure */
short newmode = 1; /* indicates that this is the first call of mlf2_ballast in the current mode (previousely, mode_start=-999 was used for that, but it was not global). It is set whenever the mode is changed (in next_mode, most commonly) */
const double g = 9.81; 

/*
 * These variables MUST be global as they are shared with the sampling code.
*/
unsigned long Sensors[NR_REAL_MODES] = {0L};
short Si[NR_REAL_MODES] = {0};


/* Declare global mode-specific parameters - Tier I
 * These parameters change in real time, depending on the current stage. E.g. the "Down" can be a part of PS ballast, or preceede a Lagrangian drift.
 * Ballast.c operates *only* with Tier I parameters
 * They will be initialized by the function initialize_mission_parameters(), called from INITFUNC = init_mode_params
 * The setup function is mission-specific, so it is included as Initialize<MISSION>.c below
 */

static struct profile Prof; 
static struct servo_p Servo_P;
static struct drift Drift;
static struct settle Settle;
static struct bocha Bocha;


/* These are "state" parameters -- not mode-related, but still Tier I*/
static struct eos EOS;
static struct drag DRAG;
static struct ballast Ballast;
static struct ctd CTD;
static struct mlb Mlb;
struct mlb *pMlb; /* pointer at Mlb */
static struct error Error;
static struct bugs Bugs;
static struct butter ButterLow;
static struct timer Timer;

/* Butterworth filter structures */
/* Each holds both filter coeff and previous values
   so a separate structure is needed for each filter*/

/* Prototypes for holding coefficients of each type*/
static struct butter ButterLow= {  /* prototype LP filter */
    50000.,   /*  Tfilt / sec  - only specify this here */
    0.,0.,0.,0.,0.,0.,0.,0.,0.   /* program fills in these */
};

#define CTDBADFLAG -100   // CTD pump is off

/* all of these are specified from above */
static struct butter ButterHi; /* prototype HP filter */
static struct butter FiltPlow;  /* filters for each variable */
static struct butter FiltPhi;
static struct butter FiltPdev;
static struct butter FiltSiglow;


/* Dummy function.  Do not remove */
void MFUNC
{
}


/* ----------------------- FLOAT MISSIONS  & PARAMETERS ---------------- */
/* Other mission files exist but not listed here */
# if defined BENGAL
# include "Missions/InitializeBengal.c"
# include "Missions/Bengal.c"
#endif
# if defined OMZ
# include "Missions/InitializeOMZ.c"
# include "Missions/OMZ.c"
#endif
# if defined ORCA
# include "Missions/InitializeORCA.c"
# include "Missions/ORCA.c"
#endif
# if defined MIZ
# include "Missions/InitializeMIZ.c"
# include "Missions/MIZ.c"
#endif
# if defined LASER
# include "Missions/InitializeLASER.c"
# include "Missions/LASER.c"
#endif
# if defined SPURS2
# include "Missions/InitializeSPURS2.c"
# include "Missions/SPURS2.c"
#endif
# if defined COLVOS16
# include "Missions/InitializeColvos16.c"
# include "Missions/Colvos16.c"
#endif


/*__________________________________________________________*/


/* Float ballasting routine starts here   */

/* This subroutine is the float ballasting subroutine to be
 * used in MLF2
 * It is driven by a Matlab simulation program,
 * or  used in the float directly
 */

/* Start set_ballast function  */
void
mlf2_ballast(double       day_time,
             double       Pressure,
             double       Temperature1,
             double       Salinity1,
             double       Temperature2,
             double       Salinity2,
             double       ballast,
             int          mode,
             int          drogue,
             double       daysec,
             int          command,
             double       *ballast_out,
             int          *mode_out,
             int          *drogue_out,
             double   *telem_out)
{
    /* ballasting variables internal to this routine */
    static double mode_start=-999;      /* time this mode started, -999 if not
                                         * started yet */

    static double last_day_time= -1000.;
    static double last_daysec=-1000.;
    static long  iout=0;   /* call counter - controls diagnostic output */
    static long isettle=0;  /* counts number of settle mode calls */
    static long idrift=0;    /* counts drift mode calls */
    static double last_profile_time= -1000;  /* time since last profile */
    static double vsave[Nav];     /* temporarily holds volume estimates for averaging */
    static double Tsave[5];          /* hold mixed layer T & S estimates for median filtering */
    static double Ssave[5];
    static double Rsave[5];       /* in situ density */
    static double Sigsave[5];     /* potential density */

	// A few parameters to hold over from the previous entry:
	static int last_mode = MODE_INIT; 
	static double last_P = 0; // it used to be that last_P=-1000 signified the first entry. last_mode = MODE_INIT is now used instead.
	static double last_Temperature = 0; 
	static double last_Salinity = 0;
	static double last_PotDensity = 0;

	//    static double surfacetime=0.;   /* time in UP mode since reaching UP.Pend */
    double mode_time;   /* time in this mode */
    static double Dsig0=0.;    /* save previous values of Dsig */
    static double Pstart=0;      /* save initial Pressure in a mode */
    static short nbadctd=0;    /* number of sequential bad CTD values */
    static double Perrtimeref=0;  /* time reference for pressure band errors */
    static short  sflag=0;     /* flag to end settle as soon as isettle increases enough */
    static double last_comm=0; /* time of last comm - used with MaxQuietDays as safety */
    static short nbadflag=0;      /* counts number of flagged CTDs */
//    static short GTDcont=0;  /* 1 if GTD is in continuous mode */
    static double Profile_time=0.; /* time since float moved away from initial position in UP/DOWN */
        
    short end_this_mode = 0; /* This can be set by Command/Timer exception or normal mode logic. If !=0, then the current mode has to terminate gracefully, and next_mode is called */

    static FILE *fout;            /* ballast log */
    double time_step_sec;  /* sampling time step  computed from data */
    double Dsig=0;
    double rho,x,y,vol0,Phi,Pdev=0,Siglow;
    double Temperature, Salinity, Density, Potemp;              // Working CTD values (can be a combination of two CTDs)
	double Pressure1, Pressure2; // Pressures @ the two CTDs
	double PotDensity1, PotDensity2; // Potential densities @ the two CTDs
    short Binit;
	double Target = 1000;	// Current target -- Settle.Target or Drift.Target depending on the mode, for ballast.txt; 1000 will be written as "0" for modes that dont' have a target
    double seek_time_sec, decay_time_sec;
    int i,j,n;


    *mode_out = mode; // by default, the mode/drogue/ballast will stay the same. Timer/Error/Command exception handlers can change it before next_mode is called
    *drogue_out = drogue;
	*ballast_out = ballast;
	*telem_out = 0;

	if (mode == MODE_INIT) {
		/* This is a dummy call from the simulator just to obtain Ptable. Do absolutely nothing.*/
		return;
	}

    if (mode==MODE_START || last_mode == MODE_INIT){ // Even though MODE_START is sent to next_mode at the beginning of each new stage, it is never returned as next mode (not anymore!). Therefore, mode==MODE_START only when sent to ballast() explicitly, such as at the mission start. Don't know why we also need to check last_mode (was last_P)
        /* first entrance */
        log_event("Ballasting (re)start\n");
        /* ballast_log headers*/
		BALLAST_LOG_HEADER;

        Drift.Voff=0;  /* Intially guess that Ballast values are OK */
        EOS.mass=EOS.Mass0;
        
        last_profile_time=day_time; //?
        Perrtimeref=0.;
		memset(vsave, 0, sizeof(vsave));  /* zero out vsave */

		// newmode = 1; // why do we need it here? next_mode should take care of it
        /* INITIALIZATION CALL */
        *mode_out = next_mode(MODE_START,day_time);
		        
		goto exit; // clean up and return
    }   /* end first entrance */

	/* Establish timing */
	time_step_sec = (day_time - last_day_time)*86400.; // duration of the previous data cycle

	if (newmode) { // mode change happened since the last call
		mode_start = day_time;
		newmode = 0;
	}
	mode_time = day_time - mode_start;
	if (last_daysec>daysec) {
		// day rollover has just occured
		last_daysec = -1; // this will enable timers and other events to happen at 00:00
	}

	if (day_time - last_comm > MaxQuietDays) {
		log_event("ERROR:   More than %3.0f days since last Comm on day %4.1f . Now day %4.1f\n", MaxQuietDays, last_comm, day_time);
		set_param_int("error_code", ERR_COMM_OVERDUE);
		*mode_out = MODE_ERROR;
		goto exit; // clean up and return
	}


	/******************  Calculate pressures ***********************/
	Pressure = Pressure + CTD.Poffset;  /* pressure at middle of float */
	PressureG = Pressure;  /* Global Pressure */
	Pressure1 = Pressure + CTD.BottomPoffset; // pressure at the bottom CTD
	Pressure2 = Pressure + CTD.TopPoffset; // pressure at the top CTD
	if (Pressure > error_P) {
		log_event("Pressure emergency %3.0f\n", Pressure);
		set_param_int("error_code", ERR_PRESSURE);
		*mode_out = MODE_ERROR;
		goto exit; // clean up and return
	}

	/******************  QC CTDs ***********************/
	/* (good CTDs will be saved before we return)  */

    /* Negative salinity check (will turn into S=0, which is still a bad CTD, but does not make density NaN)*/
    if (Salinity1 < 0)
        Salinity1 = 0.;
    if (Salinity2 < 0)
        Salinity2 = 0.;
	    
	/* Add CTD offsets */
    if (Temperature1!=0. && Salinity1!=0.){
        Salinity1=Salinity1+CTD.BottomSoffset;
        Temperature1=Temperature1+CTD.BottomToffset;
		PotDensity1 = sw_pden(Salinity1, Temperature1, Pressure1, 0);
    }
    if (Temperature2!=0. && Salinity2 !=0.){
        Salinity2=Salinity2+CTD.TopSoffset;
        Temperature2=Temperature2+CTD.TopToffset;
		PotDensity2 = sw_pden(Salinity2, Temperature2, Pressure2, 0);
    }

    /* Choose CTD values for use in ballasting (default is bottom) */
    if (CTD.which==TOPCTD
        && Salinity2 !=0 && Temperature2 !=0.){
        if (Pressure > CTD.Ptopmin ){
            Temperature=Temperature2;
            Salinity=Salinity2;
        }
        else{    /* In bubble zone, Top CTD is not good */
            /* use previous good values*/
            Temperature=last_Temperature;
            Salinity= last_Salinity;
        }
    }
    else if (CTD.which == MEANCTD
             && Pressure > CTD.Ptopmin
             && Salinity2 !=0. && Temperature2 !=0.
             && Salinity1 !=0.     && Temperature1 !=0. ){
        Salinity=(Salinity1+Salinity2)/2.;
        Temperature=(Temperature1+Temperature2)/2.;
    }
    else if (CTD.which == MAXCTD && Salinity2 > Salinity1){
        Temperature=Temperature2;
        Salinity=Salinity2;
    }
    else if (Salinity1==0 && Temperature1==0 && Pressure > CTD.Ptopmin
             && Salinity2 !=0. && Temperature2 !=0.){
        Temperature=Temperature2;  /* bottom is bad */
        Salinity=Salinity2;
    }
    else {  /* Default -- bottom CTD*/
        Temperature=Temperature1;
        Salinity=Salinity1;
    }

    if (Temperature == 0. && Salinity == 0. && CTD.BadMax>0) {  /* bad ctd value & threshold on those is set*/
        /* This is a subtle point: we don't want to run ballasting with bad CTD, but, at the same time 
           we don't want to cripple the modes that could run fine without a CTD, e.g. MODE_UP. 
           Presently, CTD failure will make the latest mode "stuck" for CTD.BadMax samples, perhaps longer if ERR_BAD_CTD is not fixed.
        */
        // printf("Z");
        nbadctd=nbadctd+1;
        if (nbadctd>CTD.BadMax) {  /* too many bad CTDs in a row, ERROR*/
            set_param_int("error_code", ERR_BAD_CTD);
			*mode_out = MODE_ERROR;
            log_event("Too many bad CTD\n");
            nbadctd=0;  /* reset in case it happens again */
			/* use previous good values (they will be re-saved!)*/
			Temperature = last_Temperature;
			Salinity = last_Salinity;
			goto exit; // clean up and return
        }
        else if (mode==MODE_COMM) { /* coming out of COM - go head and switch modes  - WHY IS THIS HERE?  */
        }
        else {/* skip this call and hope that next CTD is OK */
            if(Pressure>bottom_P)
                if (ballast<0) ballast=0;
				*ballast_out = ballast+deep_control*(Pressure-bottom_P); /* deep control. Note that if ballast<0, it is ignored in DEEP control - so that badly misballasted float doesn't sink too much. */
			goto exit; // clean up and return
        }
    }
    else { /* if not bad, then reset counter */
        nbadctd=0;
    }

	/* precompute seawater properties */
	Density = sw_dens(Salinity, Temperature, Pressure);
	PotDensity = sw_pden(Salinity, Temperature, Pressure, 0.);
	Potemp = sw_ptmp(Salinity, Temperature, Pressure, 0.);

	if (RHOMIN>0 && (PotDensity<RHOMIN || Density<RHOMIN)) {  /* Avoid divide by zero */
		set_param_int("error_code", ERR_ZERO_RHO);
		*mode_out = MODE_ERROR;
		log_event("DENSITY ERROR1  %f  %f\n", Density, PotDensity);
		PotDensity = 0;
		goto exit; // clean up and return
	}
	
	/* Set telemetry value */    
    *telem_out=Pressure;


	/******************  Compute float mass for the current ballasting cycle (including Bugs effects)  ***********************/
	// Compute bug weight. Drogue flapping went away (we'd have to do it at the end of the ballast, so it is not overridden)
	if (Bugs.stop_weight>0. && Bugs.start_weight>0.  && fabs(Bugs.start - Bugs.stop)>30.) {
		if (daysec >= Bugs.start && last_daysec<Bugs.start  && Bugs.weight == 0) {
			log_event("Sunset - bug compenstation ON %3.0f g\n", Bugs.start_weight*1000.);
			Bugs.weight = Bugs.start_weight;
		}
		if (Bugs.weight > 0.) {  /* nighttime and active */
			y = (Bugs.stop - Bugs.start);  /* duration of night/ seconds */
			if (y<0.) y = y + 86400.;
			if (y <= 0.) {
				log_event("ERROR in Bugs start & stop %4.0f %4.0f\n", Bugs.start, Bugs.stop);
				Bugs.start = -1.e6;
				y = 1e8;  /* Turn bugs off */
			}
			x = daysec - Bugs.start;
			if (x<0) x = x + 86400.;
			if (x<y) {  /* continue night */
				Bugs.weight = Bugs.start_weight + x / y*(Bugs.stop_weight - Bugs.start_weight);
				/*
				// flapping was here
				if (fmod(x + Bugs.flap_duration, Bugs.flap_interval)<Bugs.flap_duration) {
				*drogue_out = 0;  // close drogue in flap
				//printf("F");
				}
				*/
			}
			else {  /* dawn */
				log_event("Sunrise (or error) - bug compenstation OFF\n");
				Bugs.weight = 0.;
			}
		}
	}

	// Compute the float's mass for the current cycle (includes creep and bugs)
	EOS.mass = EOS.Mass0;
	if (EOS.Creep != 0 && day_time>EOS.CreepStart) {
		EOS.mass += EOS.Creep*(day_time - EOS.CreepStart);
	}
	EOS.mass += Bugs.weight;

	/* set BallastTarget from Ballast.Pgoal */
	if (Ballast.SetTarget == 8 &&
		((Pressure - Ballast.Pgoal)*(last_P - Ballast.Pgoal)<0)) {
		Ballast.Target = PotDensity;
		Pgoalset = 1;
		log_event("Set Ballast.Target %6.3f at %5.1fdbar \n", Ballast.Target - 1000., Pressure);
	}

	/* record MLB data */
	if (Mlb.record == 1) {
		if (Mlb.point == Nsave) {  /* better than just bailing */
			log_event("WARNING: Mlb array full %d points %4.1f db - add at end\n", Mlb.point, Pressure);
			Mlb.point = Nsave - 1;
		}
		Mlb.Psave[Mlb.point] = Pressure;
		Mlb.Sigsave[Mlb.point] = PotDensity;
		Mlb.point = Mlb.point + 1;
	}


	

    /* Modify sampling based on current information & mode */
    sampling(mode, day_time, daysec,mode_time);
		    
    /******************  check global timers ***********************/
    
    end_this_mode = check_timers(mode, day_time, daysec, last_daysec, mode_out);
    
    /******************  check command ***********************/
    if (command > 0)
    {
        log_event("COMMAND: %d\n", command);
        end_this_mode = handle_command(mode, day_time, command, mode_out);
        if (end_this_mode == -1)
        {
            // emergency return... Why would we want to do this?
            log_event("Command requested ballast bail out.\n");
			goto exit; // clean up and return
        }
    }
    

    /**********  Calculate the new [*ballast_out] value based on mode and check for mode end **************/
	/******************************************************************************************************/
	if (mode == MODE_PROFILE_UP || mode == MODE_PROFILE_DOWN) {// UP/DOWN profiling leg
		// Note that the control is exactly the same in MODE_PROFILE_UP and MODE_PROFILE_DOWN
		// The only parameter determining whether the float goes up or down is Prof.deltaB or W. NOT the mode ID!
		static double B0, B1; // look-back equilibrium ballast points for extrapolation, B0 is the current. Could look back further (quadratic interpolation)
		static double ExtraTime;
		static double Voff; // volume offset (for constant-speed profiling)
		// static FILE* tf; //test file ** DELETE LATER!!!
		if (mode_time == 0.) {
			Pstart = Pressure; /* save initial value */
			ExtraTime = 0.;  /* reset surface clock */
			B0 = -1; B1 = -1; // No prediction for the ballasting yet
			Voff = 0;
			log_event("PROF from P=%.1f to P=%.1f @B%+.1fcc, W%+.0fmm/s\n", Pstart, Prof.Ptarget, Prof.deltaB*1e6,Prof.W*1e3);
			// check for consistency: MODE_PROFILE_UP should have Prof.deltaB>0 or Prof.W>0
			if (mode == MODE_PROFILE_UP && (Prof.deltaB <0 || Prof.W<0 || (Prof.deltaB==0 && Prof.W==0))) {
				log_event("WARNING: MODE_PROFILE_UP inconsistent with Prof.deltaB=%.1g and Prof.W=%.1g; reset to full-speed\n", Prof.deltaB, Prof.W);
				Prof.deltaB = Prof.Bmax;//make it a normal UP
			} 
			else if(mode == MODE_PROFILE_DOWN && (Prof.deltaB >0 || Prof.W>0 || (Prof.deltaB == 0 && Prof.W == 0))) {
				log_event("WARNING: MODE_PROFILE_DOWN inconsistent with Prof.deltaB=%.1g and Prof.W=%.1g; reset to full-speed\n", Prof.deltaB, Prof.W);
				Prof.deltaB = -Prof.Bmax;//make it a normal DOWN
			}
		}

		if (fabs(Prof.deltaB) >= Prof.Bmax) {
			// with large deltaB, just revert to simple up/down, don't even bother with EOS:
			if (Prof.deltaB>0) ballast = Prof.Bmax;
			else ballast = Prof.Bmin;
		}
		else {
			// Fixed buoyancy offset or constant velocity up/down
			B1 = B0; // rotate lookback values

			// Calculate current equilibrium ballast		
			B0 = get_bocha(Pressure, Temperature, Density, 0); // bocha position. Note, that if we wanted to specify float weight offset, we could include it here!
															   // Extrapolate to get better ballast
			if (B1 >= 0) {
				// linear extrapolation
				ballast = B0 + Prof.Extrap*(B0 - B1);  // this assumes uniform stepping - ok for now
													   // quadratic extrapolation would be: 			ballast = 3 * Bx[0] - 3*Bx[1] + Bx[2];  
			}
			else {
				// constant
				ballast = B0;
			}

		if (fabs(Prof.W) > 0) {
			// constant speed profiling mode
			double Wp=0, Wr=0; // vertical velocity estimates from pressure and density
			double W = 0; // best guess W
			double N = 0; // buoyancy frequency
			//double a = 0; // acceleration ??
			double Vo;
			double Va;
			
			if (last_mode < NR_REAL_MODES) { // do not compute velocity if we are coming back from a break (COMM,ect.) 
			 // Q: how do we detect HOME?! Could just skip velocity calculation on the first pass every time... i.e. check mode_time == 0
				if (last_P > 0 && Pressure > 0 && mode_time > 0) {
					Wp = -(Pressure - last_P) / time_step_sec;
				}

				if (last_PotDensity > 0 && PotDensity > 0 && PotDensity1 > 0 && PotDensity2 > 0 && mode_time > 0) {
					N = g*(PotDensity1 - PotDensity2) / CTD.Separation / PotDensity; // this is N^2!
					if (N < 0)	N = 0;
					N = sqrt(N);
					Wr = -(PotDensity - last_PotDensity) / time_step_sec / ((PotDensity1 - PotDensity2) / CTD.Separation);
				}

				W = Wp; // For now, just use pressure W
			}
			
			Vo = get_Vdrag(Prof.W, N); // From the drag law, Volume anomaly necessary for target vel.
			Va = get_Vdrag(W, N); // From the drag law, Volume anomaly necessary for actual vel.
			Voff = Voff + ((Vo - Va) - Voff) *time_step_sec / Settle.tau; // move Voff -- this should be another tau!
			ballast += Vo+Voff;
			/*
			tf = fopen("w_tst.dat", "at");
			fprintf(tf,"%f %.1f %g %f %f %g %g %g\n", day_time,Pressure, N, Wp, Wr, Vo,Va,Voff);
			fclose(tf);
			*/
		}
		else {
				// constant offset (by now we know it is non-zero)
				ballast += Prof.deltaB; // offset the ballast relative to the equilibrium
		}
	

			if (ballast > Prof.Bmax) ballast = Prof.Bmax;
			else if (ballast < Prof.Bmin) ballast = Prof.Bmin;

		}
		// Check for reaching the target pressure:
		if ((Pressure - Prof.Ptarget)*PROF_DIR<0 && ExtraTime == 0.) {   /* start ExtraTime clock */
			ExtraTime = mode_time;  /* >0 means clock is running */
									/* printf("Start surface %6.1f %6.0f sec\n",Pressure,mode_time*86400.); */
		}

		// Activate Speedbrake if moving in the wrong direction
		if (Prof.Speedbrake == 1 && (Pressure - Pstart)*PROF_DIR>0) {
			*drogue_out = 1;  // speedbrake 
		}
		else {
			*drogue_out = Prof.drogue;
		}


		if (    /* end PROFILE */
			(ExtraTime>0 && (Prof.ExtraTime <= 0. || mode_time - ExtraTime >= Prof.ExtraTime / 86400.))  // ExtraTime logic 
			|| mode_time >= Prof.timeout / 86400.
			|| end_this_mode
			) {
			
			log_event("End PROF time %4.2f %6.1f dbar %6.0f sec \n", day_time, Pressure, mode_time * 86400);
			end_this_mode = 1; // This will prompt the call to next_mode
		}

	}
    /*******************************************************/
    else if (mode==MODE_SETTLE){  /* settle mode */

        if (mode_time==0.) { /* First entrance */
            n=0;
            Perrtimeref=mode_time;
            /* First entrance: SET SETTLE GOAL */
            if (Settle.SetTarget==1){
                Settle.Target=PotDensity;
            }
            else if (Settle.SetTarget==2){
                Settle.Target=Ballast.Target;
            }
            else if (Settle.SetTarget==3){
                Settle.Target=Drift.Target;
            }

            /* First entrance: estimate ballast point from volume and CTD */
            /* Use reference S,Th; present pressure and hull vol */
            /* NOTE - THIS IS PRESENT BALLAST POINT - NOT BALLAST POINT AT TARGET */
			Settle.B0 = get_bocha(Pressure, Temperature, Density, 0);

            ballast=Settle.B0;   /* get head start on settle ballast setting */
            log_event(
                "Start Settle P  %4.2f B %4.0f Target %6.4f Set %d Air %5.2f\n",
                Pressure,Settle.B0*1.e6,Settle.Target-1000.,Settle.SetTarget,EOS.Air*1.e6);
            isettle=0;
        } /* end First Entrance */

        /* Check for depth error during Settle mode */
        if (Error.Modes==2 || Error.Modes==3){
            if ( Pressure < Error.Pmax && Pressure>Error.Pmin) { /* OK */
                Perrtimeref=mode_time;  /* reset clock */
            }
            if (mode_time-Perrtimeref > Error.timeout/86400.) { /* Too long outside of band  */
                set_param_int("error_code", ERR_PRESSURE_RANGE);
				*mode_out = MODE_ERROR;
                log_event("ERROR: Float outside of [%4.1f %4.1f] longer than %4.0f sec\n",
                          Error.Pmin,Error.Pmax,Error.timeout);
                Perrtimeref=mode_time;  /* reset in case it happens again */
				goto exit; // clean up and return
            }
        }
        else {
            Perrtimeref=mode_time;  /* reset clock */
        }
		Target = Settle.Target; // Save settling target - for ballast.txt

        /* Seek Settle.rho only for first part of settle
           Allow rate to decay thereafter */
        Dsig=PotDensity - Settle.Target;

        // note, that if seek_time>1, it is given in seconds. Otherwise (seek_time<=1) it is given as fraction of timeout!
        if (Settle.seek_time >1){
            seek_time_sec = Settle.seek_time;
        }
        else {
            seek_time_sec = Settle.timeout*Settle.seek_time;
        }

        // note, that if decay_time>1, it is given in seconds. Otherwise (decay_time<=1) it is given as fraction of seek_time!
        if (Settle.decay_time >1){
            decay_time_sec = Settle.decay_time;
        }
        else {
            decay_time_sec = seek_time_sec*Settle.decay_time;
        }


        if (mode_time<=seek_time_sec/86400.) {
            Dsig0=Dsig;
        }
        else {
            Dsig0=Dsig*exp(-(mode_time-seek_time_sec/86400)/decay_time_sec*86400.);
        }

        /* Add fake stratification for unstratified situations */
        Dsig0=Dsig0 + (Pressure-Settle.Ptarget)*Settle.Nfake*Settle.Nfake*PotDensity/9.8;

        Settle.B0=Settle.B0
          +Dsig0/Settle.Target*EOS.V0*time_step_sec/Settle.tau;  /* Seeking */

        if (Settle.B0<Settle.Bmin)Settle.B0=Settle.Bmin;   /* Limit Seeking amplitude */
        if (Settle.B0>Settle.Bmax) Settle.B0=Settle.Bmax;

        ballast = Dsig0/Settle.Target*EOS.V0*Settle.beta + Settle.B0; /*PseudoComp*/

        if (mode_time<Settle.drogue_time/86400.)
			*drogue_out =1;         /* remain open for drogue_time  */
        else 
			*drogue_out =0; 

        /* Compute EOS.Vol0 */
		vol0 = get_V0(Pressure, Temperature, Density, ballast);
        
        if (Settle.nav>Nav){Settle.nav=Nav;} /* prevent over/underflow */
        if (Settle.nav<1){Settle.nav=1;}
        vsave[isettle%Settle.nav]=vol0;  /* Save recent volume estimates */
        ++isettle;

        /* END SETTLE */
        if ( daysec >= Settle.secOday && last_daysec < Settle.secOday ){
            sflag=1;  /* crossed time - set flag to end settle soon */
            log_event("%4.2f End Settle soon - secOday\n",day_time);
        }
        if ( (mode_time>Settle.timeout/86400. && isettle>Settle.nav )  /* timeout */
             || ( sflag==1 && isettle>Settle.nav )  /* secOday */
             || Settle.nskip==0
             || Settle.timeout <0.
             || end_this_mode){
            sflag=0;

            if ( fabs(Dsig*EOS.V0) < Settle.weight_error && isettle>Settle.nav) /* good settle ??*/
            {
                printf("### M=%f, Ro=%f, B=%e A=%e, P=%f, C=%e, TE=%e, T=%f, Tr=%f => V=%e\n",
                       EOS.mass,Density-1000, ballast,EOS.Air,Pressure,EOS.Compress,   EOS.Thermal_exp,Temperature,EOS.Tref,vol0);
                printf("### M/rho=%f, v-B=%f, v-Air=%f \n",
                       EOS.mass/Density,EOS.mass/Density-ballast,EOS.mass/Density -ballast - EOS.Air*EOS.Pair/(EOS.Pair+Pressure));
                printf("### chi*P=%f, Texp=%f\n",
                       EOS.Compress*Pressure, EOS.Thermal_exp*(Temperature-EOS.Tref));

                vol0=filtmean(vsave,Settle.nav);  /* average Nav values */
                x=0;  /* Compute Error */
                for (i=0;i<Settle.nav;++i)
                    x=x+pow(vol0-vsave[i],2.);
                x=sqrt(x/Settle.nav);  /* Stdev of Volume estimates */
                Ballast.Vdev=x;
                if ( x<Settle.Vol_error) {   /* Settling OK  */
                    rho=PotDensity;
                    log_event("%4.2f Settled P %4.2f  Sig0 %6.4f  dweight %4.1f Vol0 %7.1f (%5.2f) B %6.2f\n",
                              day_time,Pressure,rho-1000, Dsig*EOS.V0*1000, vol0*1e6,x*1.e6,ballast*1.e6);
                    if (Settle.SetV0){
						BALLAST_LOG_ENTRY;
                        EOS.V0=vol0;
						BALLAST_LOG_ENTRY;
                        log_event("Set EOS.V0 from Settle %f\n",EOS.V0*1e6);
                    }
                    if (Ballast.SetTarget==4){  /* set from end of Settle */
                        Ballast.Target=PotDensity;
                        log_event("Setting Ballast.Target from end of Settle (%6.3f)\n",Ballast.Target-1000.);
                    }
                }
                else {   /* Settle NOT OK - do not use  */
                    log_event("Not Settled: P %4.2f  Sig0 %6.4f  dweight %4.1f Vol0 %7.1f (%5.2f) B %6.2f\n",
                            Pressure,PotDensity-1000, Dsig*EOS.V0*1000, vol0*1e6,x*1.e6,ballast*1e6);
                }
            }
            else
            {
                log_event("Settle not converged: P %4.2f Dsig %6.3f Dweight %4.1f\n",
                        Pressure,Dsig,Dsig*EOS.V0*1000.);
            }
            end_this_mode = 1; // This will prompt the call to next_mode

        }
    }
    /*******************************************************/
    else if (mode==MODE_SERVO_P) {/* SERVO_P mode */
        static double ballast_request;
        static double Bspeed; /* bocha moving speed (applies below Pgoal) */
        static short Pgoal_reached; // 
        if (mode_time==0.){
            Pstart=Pressure; /* save initial value */
            log_event("Servo_P from P=%5.1f bounds %5.1f %5.1f \n",Pstart,Servo_P.Pmin, Servo_P.Pmax);
			*drogue_out =Servo_P.drogue;
            Bspeed = Servo_P.Bspeed;
            Pgoal_reached = 0; // Pgoal has not been reached
            ballast_request = ballast;
         }
        if (Pgoal_reached)
        {   // reduce Bspeed if Pgoal has been reached
            Bspeed = Bspeed*(1 - time_step_sec / Servo_P.BspeedEtime);
        }

        if (Pressure > Servo_P.Pgoal)
        { // we are below Pgoal here - keep pushing Bocha out
            ballast_request = ballast_request + Bspeed*time_step_sec; 
        }
        else // above Pgoal, push away from surface quickly by reversing Bspeed 
        { 
            ballast_request = ballast_request - Servo_P.BspeedShallow*time_step_sec;  
            printf("v");
            Pgoal_reached = 1;
        }

        ballast = ballast_request; // we have to keep ballast_request separate from ballast, because the latter can accumulate under-driving errors

        
      
               
        if(    /* end SERVO_P*/
            Pressure<Servo_P.Pmin || Pressure>Servo_P.Pmax
           || mode_time >=Servo_P.timeout/86400.
           || end_this_mode)
        {
            log_event("End Servo_P time %4.2f %6.1f dbar %6.0f sec \n",day_time,Pressure,mode_time*86400);
                
            end_this_mode = 1; // This will prompt the call to next_mode
        }
    }
    /*******************************************************/
    else if (mode == MODE_BOCHA) {/* BOCHA mode */
        if (mode_time == 0.){
            Pstart = Pressure; /* save initial value */
            log_event("Fix B=%f for %4.0fs\n", Bocha.B*1e6,Bocha.timeout);
			*drogue_out = Bocha.drogue;
        }

        if (fabs(ballast - Bocha.B) <= Bocha.Btol)
           end_this_mode = 1; // close enough, will end the mode
        else
           ballast = Bocha.B; // fixed bocha - simple as that

        if (    /* end BOCHA */
            mode_time >= Bocha.timeout / 86400.
            || end_this_mode)
        {
            log_event("End Bocha time %4.2f %6.1f dbar %6.0f sec \n", day_time, Pressure, mode_time * 86400);
			if (Bocha.SetV0) {  /* compute EOS.V0 at end of drift assuming neutral */
				BALLAST_LOG_ENTRY;
				EOS.V0 = get_V0(Pressure, Temperature, Density, ballast); // shouldn't we need to use average, as in Settle?
				BALLAST_LOG_ENTRY;
				log_event("Set EOS.V0 from Bocha End %f \n", EOS.V0*1.e6);
			}
            end_this_mode = 1; // This will prompt the call to next_mode
        }
    }
    /*******************************************************/
    else if (mode == MODE_DRIFT_ISO || mode  == MODE_DRIFT_ML ||  mode == MODE_DRIFT_SEEK) { /* Drift Modes  */
        if (mode_time>Drift.closed_time/86400.) 
			*drogue_out =1;
        else    
			*drogue_out =0;

        if(mode_time==0.){  /* First entry */
            log_event("Drift Start: EOS.V0 %f\n",EOS.V0*1e6);
            Perrtimeref=mode_time;
            if (Drift.VoffZero==1){
                /* Assume EOS.V0 is correct and float is neutral*/
                Drift.Voff=0.;
            }

            /* initialize Median filter with current values*/
            if(Drift.median==1){
                for (i=0;i<5;++i){
                    Tsave[i]=Potemp;
                    Ssave[i]=Salinity;
                    Sigsave[i]=PotDensity;
                }
                idrift=0;
            }
        }/* end first entry */

        /* Check for depth error during Drift mode */
        if (Error.Modes==1 || Error.Modes==3){
            if ( Pressure < Error.Pmax && Pressure>Error.Pmin) { /* OK */
                Perrtimeref=mode_time;  /* reset clock */
            }
            if (mode_time-Perrtimeref > Error.timeout/86400.) { /* Too long outside of band  */
                set_param_int("error_code", ERR_PRESSURE_RANGE);
				*mode_out = MODE_ERROR;
                log_event("ERROR: Float outside of [%4.1f %4.1f] longer than %4.0f sec\n",
                          Error.Pmin,Error.Pmax,Error.timeout);
                Perrtimeref=mode_time;  /* reset in case in happens again */
				goto exit; // clean up and return
            }
        }
        else {
            Perrtimeref=mode_time;  /* reset clock */
        }

        /* GET DENSITY FOR BALLASTING */
        if (Drift.median==1){  /* use 5 point median filter */
            /* Update filter values */
            ++idrift;
            j=idrift%5;
            Tsave[j]=Potemp; /* Theta */
            Ssave[j]=Salinity;
            Sigsave[j]=PotDensity;
            for (i=0;i<5;++i){ /* get saved density at current P */
                x=sw_temp(Ssave[i],Tsave[i],Pressure,0.); /* T at this P */
                Rsave[i]=sw_dens(Ssave[i],x,Pressure);
            }
            Ballast.S0=opt_med5(Ssave);
            Ballast.TH0=opt_med5(Tsave);
            Ballast.P0=Pressure;
            Ballast.rho0=opt_med5(Sigsave);
            rho=opt_med5(Rsave); /* Median filtered rho at ML S,Th, Current P  */
        }
        else{  /* No median filter */
            Ballast.S0=Salinity;
            Ballast.TH0=Potemp;
            Ballast.P0=Pressure;
            Ballast.rho0=PotDensity;
            rho=Density;
        }
        
        /* Use this density in ballasting equation */
        if (RHOMIN>0 && rho<RHOMIN){  /* Avoid divide by zero */
            set_param_int("error_code", ERR_ZERO_RHO);
			*mode_out =MODE_ERROR;
            log_event("Density ERROR2 %f\n",rho);
			goto exit; // clean up and return
        }

        /* Filtered Pressure and density */
        if (mode_time==0){
            /* Initialize Butterworth filters (in case they have changed) */
            /* note that ButterLow.Tfilt is master variable satellite setable */
            log_event("INITIALIZE FILTERS  Tfilt %4.0f",ButterLow.Tfilt);
            ButterLowCoeff(ButterLow.Tfilt, &ButterLow);
            ButterHiCoeff(ButterLow.Tfilt, &ButterHi);
            log_event("-> %4.0f\n",ButterLow.Tfilt);
            Binit=1;  /* reset at start of each drift */
        }
        else Binit=0;
        /* Run filters */
        /* Plow=Bfilt(&FiltPlow,Pressure,Pressure,Binit,ButterLow);  Low Pass P */
        Phi=Bfilt(&FiltPhi,Pressure,0.,Binit,ButterHi);        /* High Pass P */
        Pdev=Bfilt(&FiltPdev,fabs(Phi),0.,Binit,ButterLow); /* Low Pass P deviations */
        Siglow=Bfilt(&FiltSiglow,Ballast.rho0,Ballast.rho0,Binit,ButterLow); /* Low Pass Potential Density */

        /* CHOOSE DRIFT MODE VARIANT */
        if ( Pressure<Ballast.MLthreshold * Pdev  ||  Pressure < Ballast.MLmin
             ||( Pressure>Ballast.MLtop && Pressure<Ballast.MLbottom )){
            mode=MODE_DRIFT_ML;     /* ML MODE */
        }
        else { /* ISO MODES */
            if(  Pressure > Drift.seek_Pmin   &&  Pressure <Drift.seek_Pmax  &&
                 Pressure > Ballast.SEEKthreshold*Pdev){ /* with seek */
                mode=MODE_DRIFT_SEEK;
            }
            else {    /* without seek */
                mode=MODE_DRIFT_ISO;
            }
        }
		*mode_out = mode; // update this, so that the DRIFT mode flavor changes are logged
//#if 0
//if (iout % 5 ==0)
//    printf("BBB   %6.4f  %5.1f  %5.1f %5.1f  %5.1f %d\n",day_time,Pressure,Plow,Phi,Pdev,mode);
//#endif

/* SET & MODIFY TARGET DENSITY */
        if (mode_time==0) {
            /* SET DRIFT GOAL */
            if (Drift.SetTarget==1){
                Drift.Target=PotDensity;
                log_event("Set Drift.Target to PotDensity (%6.3f)\n",Drift.Target-1000);
            }
            else if (Drift.SetTarget==2){
                Drift.Target=Ballast.Target;
                log_event("Set Drift.Target to Ballast.Target (%6.3f)\n",Drift.Target-1000);
            }
            else if (Drift.SetTarget==3){
                Drift.Target=Settle.Target;
                log_event("Set Drift.Target to Settle.Target (%6.3f)\n",Drift.Target-1000);
            }
        }
/* change when in ML */
        if ( mode==MODE_DRIFT_ML ){
            if(Ballast.MLsigmafilt==1){
                Drift.Target=Siglow;
            }
            else {
                Drift.Target=Ballast.rho0;
            }
        }
		Target = Drift.Target; // Save drifting target - for ballast.txt
/* Pot. Density Anomaly - filtered or not */
        Dsig=(Drift.Target-Ballast.rho0);

/* ISOPYCNAL SEEKING  */
/* Seek isopycnal with timescale Drift.isotime */
        if( mode==MODE_DRIFT_SEEK){
            Drift.Voff=Drift.Voff -Dsig/Drift.iso_time*time_step_sec/rho*EOS.V0;
            if (Drift.Voff<Drift.Voffmin)Drift.Voff=Drift.Voffmin;
            Dsig0=Dsig;  /* save isopycnal deviation (I'm not sure why) */
        }

		/*
		// disabled for now
// estimate average pressure in next time interval
        if (mode_time>0) next_P=Pressure+(Pressure-last_P)/2;
        else next_P=Pressure;
        if (next_P<0) next_P=0;  // could be fancier than this 
		*/

/* BALLAST */
        ballast = get_bocha(Pressure, Temperature,rho,0) +Drift.Voff; // bocha position. Do we need filtered pressure here? (density /may/ be filtered, hence "rho" rather than "Density"!)
          
/* Add Pseudo-compressibility  */
        ballast=ballast -Drift.iso_Gamma*Dsig*EOS.V0/rho;

/* END DRIFT  */
        if( ( (Drift.timetype==1) /* time since end of last drift */
                     && day_time-last_profile_time>Drift.time_end_sec/86400.)
            || ( (Drift.timetype==2) /* fraction of day in seconds 0-86400 */
                     && daysec >= Drift.time_end_sec  && last_daysec < Drift.time_end_sec )
            || mode_time > Drift.timeout_sec/86400   /* duration of drift (always active) */
            || end_this_mode
            ){
			log_event("End Drift time %4.2f %6.1f dbar %6.0f sec\n", day_time, Pressure, mode_time * 86400);

            last_profile_time=day_time;

            if (Ballast.SetTarget ==5){ /* set Ballast at end of Drift */
                Ballast.Target=PotDensity;
                log_event("Set Ballast.Target from the end of drift (%6.3f)\n",Ballast.Target-1000.);
            }
            if (Drift.SetV0){  /* compute EOS.V0 at end of drift assuming neutral */
				BALLAST_LOG_ENTRY;
				EOS.V0 = get_V0(Pressure, Temperature, Density, ballast); // shouldn't we need to use average, as in Settle?
				BALLAST_LOG_ENTRY;
                Drift.Voff=0;
                log_event("Set EOS.V0 from Drift End %f \n",EOS.V0*1.e6);
            }

            end_this_mode = 1; // This will prompt the call to next_mode
        }
    }/* end drift mode code */
    /*******************************************************/
    else if(mode==MODE_COMM) { /* Return from Comm mode */
        last_comm=day_time; /* reset Comm timer */
        end_this_mode = 1; // This will prompt the call to next_mode
    }
    /*******************************************************/
    else if (mode == MODE_XFER) { /* Return from AUX file transfer mode */
        // just like MODE_COMM
        log_event("AUX files remaining: %d\n", get_param_as_int("aux:files_remaining"));
        end_this_mode = 1; // This will prompt the call to next_mode
    }
    /*******************************************************/
    else if (mode==MODE_DONE){
        log_event("DONE!\n");
        newmode = 1; // do we care anymore?
    }
    else if (mode == MODE_SLEEP){ /* Return from Sleep mode */
        log_event("ballast:end SLEEP\n");
        end_this_mode = 1; // This will prompt the call to next_mode
    }
    else if (mode==MODE_ERROR){
        /*  How this can happen?? Don't call next mode - just let it do a COMM     */
    }
    else{
        log_event("Cant Get here - NonExistant Mode %d\n",mode);
        set_param_int("error_code", ERR_INVALID_MODE);
		*mode_out = MODE_ERROR;
		goto exit; // clean up and return
    }
    /********************* This is the end of mode branching **********************************/
    
    /* Check if we need to switch to a new mode (the old one have already been wrapped up gracefully) */
    if (end_this_mode)
    {
        // Note that by now mode_out may have changed by one of the exception handlers - this is Ok! We need to pass *that* to next_mode
        *mode_out = next_mode(*mode_out, day_time);
    }

	/**** Do some clean-up before returning ****/
exit: 
    /* DEEP CONTROL */
    if (Pressure > bottom_P){
        if (ballast<0)ballast=0;
        ballast=ballast+deep_control*(Pressure-bottom_P); // Note that if ballast<0, it is ignored in DEEP control - so that badly misballasted float doesn't sink too much.
        printf("D");
    }
    /* SHALLOW CONTROL */
    if (Pressure<top_P && (is_settle(mode) || is_drift(mode))){
        ballast=ballast-(top_P-Pressure)*shallow_control;
        printf("S");
    };

	/* Write to syslog (periodically and on error) */
    if (iout%NLOG==0 || *mode_out==MODE_ERROR){
        log_event(
            "%5.3f P %5.2f Ball %6.1f Mode %d Voff:%7.2f Target %6.3f Sig0 %6.3f \n",
            day_time,Pressure,ballast*1.e6,mode,
            Drift.Voff*1.e6,Target-1000.,PotDensity-1000.);
    }

    /* write Ballast log information (periodically and on mode change)  */
#ifdef SIMULATION
    if ((iout %NLOG == 1 || newmode) && day_time< LOGDAYSSIM){
#else
    if (iout %NLOG == 1 || newmode){
#endif
		BALLAST_LOG_ENTRY;
        }

        /* final assignments and updates before returning to calling program*/
        ++iout;
		// *mode_out has already bean assigned
		// *drogue_out has already bean assigned
        *ballast_out=ballast; // this has NOT been assigned: for historic reasons, ballast itself is modified

		/* save current values for the next cycle*/
		last_mode = mode;
        last_day_time=day_time;
        last_daysec=daysec;
		last_P = Pressure;
		last_Temperature = Temperature;
		last_Salinity = Salinity;
		last_PotDensity = PotDensity;
    }

/********************* END OF MAIN CODE - NOW SUBROUTINES **********************************/
/* get_bocha - compute bocha position needed to acheive given float weight in water.
	Neutral buoyancy (equilibrium bocha or B0) is a particular case (weight=0)
*/
   
double get_bocha(double P, double T, double rho, double weight)
    {
		double vol, B0;

		// Avoid divide by zero if P<=-10
		if (P <= -EOS.Pair) P = -EOS.Pair + 0.01;

		// Float volume (minus the piston)
		vol = EOS.V0
			- EOS.Compress*P*EOS.V0
			+ EOS.Thermal_exp*(T - EOS.Tref)*EOS.V0
			+ EOS.Air*EOS.Pair / (EOS.Pair + P);
		B0 = (EOS.mass - weight) / rho - vol;
    return (B0 );
    }
    
/* get_V0 - compute V0 assuming float neutral */
double get_V0(double P, double T, double rho, double bocha)
    {
        double v;
		// Avoid divide by zero if P<=-10
		if (P <= -EOS.Pair) P = -EOS.Pair + 0.01;

        v = EOS.mass/rho - bocha - EOS.Air*EOS.Pair/(EOS.Pair+P);
        v = v / (1 - EOS.Compress*P + EOS.Thermal_exp*(T - EOS.Tref) );
        return (v);
    }

/* get_Vdrag - find volume anomaly given the vertical velocity and the drag law*/
double get_Vdrag(double W, double N)
{
	/* 
	The drag law is DragForce = -(0.5*Cd*Area*W*|W| + Area*L*N*W)*Density,
	where W is vertical velocity, N is buoyancy frequency.
	In terms of volume anomaly necessary to move at a given vertical velocity, 
	Vdrag =  (0.5*Cd*Area*W*|W| + Area*L*N*W)/g
	*/
	return (0.5*DRAG.Cd*DRAG.Area*W*fabs(W) + DRAG.Area*DRAG.L*N*W) / g;
}
//=========================================================================

/* returns mean of array X with maximum and minimum values removed */
    double filtmean(double *X, int N)
    {
        double      min,max,mean;
        int         i;

        /* log_event("Filtering %d elements at 0x%08lx\n", N, (unsigned long)X);*/

        mean=0.;
        if (N<=2) return X[0];   /* Default return */
        min=1e10;
        max=-1e10;
        for (i=0;i<N;++i){
            /* log_event("X[%d] = %g\n", i, X[i]);*/
            if (X[i]<min)
                min=X[i];
            else if (X[i]>max)
                max=X[i];
            mean=mean+X[i];
        }
        mean=(mean-min-max)/(N-2);
        return mean;
    }
/*%=========================================================================
 */
/* Second order Butterworth filter function */
/*                 this filter struc,  input data, initalize?, protoype filter */
    double Bfilt(struct butter *B, double X, double YI, short init, struct butter P)
    {
        double Y;
        if (init==1){  /* initialize filter */
            /* saved values all initialized to first value */
            B->Xp=X;
            B->Xpp=X;
            B->Yp=YI;    /* output values to YI */
            B->Ypp=YI;
            /* coefficients set to prototype values
             * This allows coefficients to be changed by satellite */
            B->A2=P.A2;   B->A3=P.A3;
            B->B1=P.B1;     B->B2=P.B2;   B->B3=P.B3;
            B->Tfilt=P.Tfilt;
            log_event("COEFF:%5.0f %e %e %e %e %e\n",B->Tfilt,B->A2,B->A3, B->B1, B->B2,B->B3);
        }

        /* Evaluate filter */
        Y=B->B1*X  +   B->B2*B->Xp  +  B->B3*B->Xpp
          - B->A2*B->Yp   -   B->A3*B->Ypp;
        /* save values */
        B->Ypp=B->Yp;
        B->Yp=Y;
        B->Xpp=B->Xp;
        B->Xp=X;
        return Y;
    }
/*------------------------------------------------------------------------------------ */
/* returns density of Mixed Layer Base from profile */
/* profile should be  downcast, ending in long monotonic section */
    double getmlb( struct mlb * X)  /* X is pointer to structure */
    {
        double Sigmlb,Pp,dP,Sigmin,Sigmax;
        short Pindex[Nsave],N,Np;
        int imax, i,j;
        /* Structure X:  *save are input data, shallow to deep
         *bin are computed equally spaced
         */

        printf("getmlb start\n");
        imax=X->point-1;  /* maximum index */
        if (imax>Nsave-1){log_event("ERROR: getmlb Err 1 %d\n",imax);
            return -1;}

        /* make monotonic by removing points, bottom up */
        Pp=X->Psave[imax];
        j=imax;
        Pindex[imax]=imax;
        for (i=imax-1;i>=0;--i){    /* find indices of monotonic points */
            if (X->Psave[i]<Pp){  /* good */
                j--;
                if(j<0 || j>Nsave-1){log_event("ERROR: getmlb Err 2 %d\n",j);
                    return -1;}
                Pindex[j]=i;
                Pp=X->Psave[i];
            }
        }
        for (i=imax;i>=j;--i){     /*delete nonmonotonic data and resave array */
            X->Psave[i]=X->Psave[Pindex[i]];
            X->Sigsave[i]=X->Sigsave[Pindex[i]];
        }
        Sigmin=2000;Sigmax=0;  /* also find min and max */
        for (i=0; i<(imax - Pindex[j]); ++i){
            if (i>Nsave-1){log_event("ERROR: getmlb Err 3 %d\n",i);return -1;}

            X->Psave[i]=X->Psave[i+ Pindex[j]];
            X->Sigsave[i]=X->Sigsave[i+ Pindex[j]];
            /* printf("%d  %f %f\n",i,X->Psave[i],X->Sigsave[i]);*/
            if (X->Sigsave[i] >Sigmax)Sigmax=X->Sigsave[i];
            if (X->Sigsave[i] <Sigmin)Sigmin=X->Sigsave[i];
        }
        imax=i;  /*new length */
        /* printf("min %f max %f\n",Sigmin,Sigmax);*/
        log_event("mlb raw data:%d points %5.1f  to %5.1fdb\n",imax,X->Psave[0],X->Psave[imax]);

        /* now grid onto uniform grid */
        for (i=0;i<Ngrid-1;++i){ /* fill grid */
            X->Pgrid[i]=i*X->dP;
            X->Siggrid[i]=-1.;  /* fill with bad flags */
        }
        j=0; /* data point index */
        for (i=0;i<Ngrid-1;++i){ /* for each grid point */
            /* move data to grid, check for overflow */
            while(X->Psave[j+1] < X->Pgrid[i] && j<imax-1 && j<=Nsave-2){++j;}
            if (j>=imax || j>Nsave-2){break;} /* EOD */
            if ( X->Psave[j] >= X->Pgrid[i] ){continue;} /* no data for this grid */

            if (X->Psave[j] < X->Pgrid[i] &&  X->Psave[j+1] >= X->Pgrid[i]){ /* if data */
                dP=(X->Psave[j+1]-X->Psave[j]);
                if (dP<=0) /* check for divide by zero */
                {log_event("ERROR: getmlb Err 4 %f\n",dP);return -1;}

                X->Siggrid[i]=X->Sigsave[j] +  /* interpolate to grid */
                  (X->Sigsave[j+1]-X->Sigsave[j])/dP*(X->Pgrid[i] - X->Psave[j]);
            }
        }
        /*
         * for (i=0;i<Ngrid-1;++i){
         * printf("%d  %f %f\n",i,X->Pgrid[i],X->Siggrid[i]);
         * }*/
        /* count number of points, must be >Nmin */
        N=0;
        for(j=0;j<Ngrid;++j){
            if(X->Siggrid[j]>0)++N;
        }
        log_event("mlb gridded data:%d points %5.1f to %5.1fdb\n",N,X->Pgrid[0],X->Pgrid[N-1]);
        if (N<X->Nmin){log_event("ERROR: getmlb Err 5 %d\n",N);return -1;}

        /* Apply MLB algorithm */
        if (X->dSig==0){log_event("ERROR: getmlb Err 6 %f\n",X->dSig);return -1;}
        j=0; /* safety index */
        imax=(int)(2*fabs(Sigmax-Sigmin)/X->dSig);  /* max loops = twice estimated */

        Sigmlb=Sigmin - 3*X->dSig;
        N=0;Np=0;
        while( N>=Np && Sigmlb<Sigmax){
            ++j;
            if(j>imax){log_event("ERROR: getmlb Err 7 %d\n",j);return -1.;}
            Sigmlb=Sigmlb + X->dSig;
            Np=N;
            N=0;
            for (i=0; i<Ngrid-1;++i){ /*get number of points */
                if( fabs( X->Siggrid[i]-Sigmlb) < X->dSig
                    && X->Siggrid[i]>0)++N;
            }
        }
        if (Sigmlb>=Sigmax || Sigmlb< Sigmin ){
            log_event("ERROR: getmlb Err 8 %f\n",Sigmlb);return -1;}

        return Sigmlb;
    }

/*------------------------------------------------------------------------------------ */
/* returns potential density at Z from logged profile  */
/*    first data that spans the depth is used  */
/*  if this is not found, returns mean density  */
/*  if error, return -1 */
    double z2sigma( struct mlb * X, double Z)  /* X is pointer to structure */
    {
        double Sigmean,x1,x0,S1,S0,xmin;
        int imax, i;
        /* Structure X:  *save are input data
         */

        log_event("z2sigma seeking %6.3f\n",Z);
        imax=X->point-1;  /* maximum index */
        if (imax>Nsave-1 || imax<2 ){log_event("ERROR: z2sigma: bad length %d\n",imax);
            return -1;}
        Sigmean=0;
        for (i=1; i<=imax ; ++i){
            if (i>Nsave-1){log_event("ERROR: z2sigma: index error %d\n",i);return -1;}
            x0=X->Psave[i-1];  /* two points near i */
            x1=X->Psave[i];
            if(i==1){xmin=x0;}
            S1=X->Sigsave[i];
            S0=X->Sigsave[i-1];
            Sigmean=Sigmean-1000.+S1;
            if ( (x0>Z && Z>=x1) || (x1>Z && Z>=x0)) { /* found a point */
                if (x1==x0){
                    return (S1+S0)/2.;
                }
                else{
                    return S1+(S0-S1)/(x0-x1)*(Z-x1);
                }
            }
        }
        Sigmean=1000.+Sigmean/(imax-1);
        log_event("WARNING: z2sigma cannot find z=%6.3f in [%6.3f %6.3f], returning mean (%6.3f)\n",Z, xmin,x1, Sigmean);
        return(Sigmean);
    }

/*------------------------------------------------------------------------------------ */



/*
 * Parameter table initialization.  See ptable.c for details on how
 * the parameter table works.
 *
 * Note that it runs in Matlab as well now!
 */

/*
 * Initialize the mode-control parameters.
 */
    INITFUNC(init_mode_params)
    {


        // memset(Sensors, 0, sizeof(Sensors)); do not need

        Sensors[MODE_PROFILE_UP] =   PROF_SENSORS;
        Sensors[MODE_PROFILE_DOWN] = PROF_SENSORS;
        Sensors[MODE_SETTLE] = SETTLE_SENSORS;
        Sensors[MODE_DRIFT_ISO] = DRIFT_SENSORS;
        Sensors[MODE_DRIFT_ML] = DRIFT_SENSORS;
        Sensors[MODE_DRIFT_SEEK] = DRIFT_SENSORS;
        Sensors[MODE_SERVO_P] = DRIFT_SENSORS;
        Sensors[MODE_BOCHA] = DRIFT_SENSORS;

        // memset(Si, 0, sizeof(Si)); do not need
        // Sampling intervals (Si) are now set in initialize_mission_parameters()


        /* Create Ptable links to the mode-specific (Tier I) parameters. The rest are in initialize_mission_parameters*/
        /* These macros expand to add_param("Down.Pmax",	PTYPE_DOUBLE, &Down.Pmax), etc.  */
        /* Use ADD_PTABLE_D for PTYPE_DOUBLE, ADD_PTABLE_L for PTYPE_LONG, and ADD_PTABLE_S for PTYPE_SHORT.*/
		/* We also use macros that add all the structure elements to Ptable at once (e.g., ADD_PTABLE_DOWN), see ballast.h*/

		/* Tier-1 Mode control structures -- should these be exposed in Ptable at all??? */
		ADD_PTABLE_PROFILE(Prof);
		ADD_PTABLE_SERVO_P(Servo_P);
		ADD_PTABLE_SETTLE(Settle);
		ADD_PTABLE_BOCHA(Bocha);
		ADD_PTABLE_DRIFT(Drift);
		ADD_PTABLE_BALLAST(Ballast);


        /* intervals and samping */
		/* I'd say the names of the Ptable entries should be "sensors[Up]", "sensors[Down]", etc. -AS*/
        add_param("Up:sensors", PTYPE_LONG, &Sensors[MODE_PROFILE_UP]);
        add_param("Down:sensors", PTYPE_LONG, &Sensors[MODE_PROFILE_DOWN]);
        add_param("Driftiso:sensors", PTYPE_LONG, &Sensors[MODE_DRIFT_ISO]);
        add_param("Driftml:sensors", PTYPE_LONG, &Sensors[MODE_DRIFT_ML]);
        add_param("Driftseek:sensors", PTYPE_LONG, &Sensors[MODE_DRIFT_SEEK]);
        add_param("Settle:sensors", PTYPE_LONG, &Sensors[MODE_SETTLE]);
        add_param("Servo_P:sensors", PTYPE_LONG, &Sensors[MODE_SERVO_P]);
        add_param("Bocha:sensors", PTYPE_LONG, &Sensors[MODE_BOCHA]);

        add_param("Up:si", PTYPE_SHORT, &Si[MODE_PROFILE_UP]);
        add_param("Down:si", PTYPE_SHORT, &Si[MODE_PROFILE_DOWN]);
        add_param("Driftiso:si", PTYPE_SHORT, &Si[MODE_DRIFT_ISO]);
        add_param("Driftml:si", PTYPE_SHORT, &Si[MODE_DRIFT_ML]);
        add_param("Driftseek:si", PTYPE_SHORT, &Si[MODE_DRIFT_SEEK]);
        add_param("Settle:si", PTYPE_SHORT, &Si[MODE_SETTLE]);
        add_param("Servo_P:si", PTYPE_SHORT, &Si[MODE_SERVO_P]);
        add_param("Bocha:si", PTYPE_SHORT, &Si[MODE_BOCHA]);

        /* misc. ungrouped parameters */
        ADD_PTABLE_D(   shallow_control  );
        ADD_PTABLE_D(   deep_control  );
        ADD_PTABLE_D(   top_P  );
        ADD_PTABLE_D(   bottom_P  );
        ADD_PTABLE_D(   error_P  );
        ADD_PTABLE_S(   stage  );

        ADD_PTABLE_D(   telem_Step  );
        // ADD_PTABLE_S(   commhome  );
        ADD_PTABLE_D(   Home_days  );
        ADD_PTABLE_D(   MaxQuietDays  );
        ADD_PTABLE_D(   sleepduration  );

        /* Butterworth filter parameter - sets both hi and low */
        ADD_PTABLE_D(   ButterLow.Tfilt  );

        ADD_PTABLE_S(   Error.Modes  );
        ADD_PTABLE_D(   Error.Pmin  );
        ADD_PTABLE_D(   Error.Pmax  );
        ADD_PTABLE_D(   Error.timeout  );
        
		
		ADD_PTABLE_EOS(EOS);
		ADD_PTABLE_DRAG(DRAG);
		ADD_PTABLE_CTD(CTD);

        ADD_PTABLE_S(   Mlb.go  );
        ADD_PTABLE_S(   Mlb.Nmin  );
        ADD_PTABLE_D(   Mlb.dP  );
        ADD_PTABLE_D(   Mlb.dSig  );
        ADD_PTABLE_D(   Mlb.Sigoff  );
        
        /* Bug parameters */
        ADD_PTABLE_D(   Bugs.start  );
        ADD_PTABLE_D(   Bugs.stop  );
        ADD_PTABLE_D(   Bugs.start_weight  );
        ADD_PTABLE_D(   Bugs.stop_weight  );
        ADD_PTABLE_D(   Bugs.flap_interval  );
        ADD_PTABLE_D(   Bugs.flap_duration  );

        /* global timers */
        ADD_PTABLE_S(   Timer.enable  );
		/* Ideally, we'd want to write it like this
			add_array("Timer.time",PTYPE_DOUBLE, Timer.time, NTIMERS);
			add_array("Timer.command", PTYPE_SHORT, Timer.command, NTIMERS);
			add_array("Timer.nskip", PTYPE_SHORT, Timer.nskip, NTIMERS);
		but it'll require Mike to add a function to his Ptable library.
		For now...*/
		ADD_PTABLE_D(	Timer.time[0]	);
		ADD_PTABLE_D(	Timer.time[1]	);
		ADD_PTABLE_D(	Timer.time[2]	);
		ADD_PTABLE_D(	Timer.time[3]	);
		ADD_PTABLE_S(	Timer.command[0]);
		ADD_PTABLE_S(	Timer.command[1]);
		ADD_PTABLE_S(	Timer.command[2]);
		ADD_PTABLE_S(	Timer.command[3]);
		ADD_PTABLE_S(	Timer.nskip[0]	);
		ADD_PTABLE_S(	Timer.nskip[1]	);
		ADD_PTABLE_S(	Timer.nskip[2]	);
		ADD_PTABLE_S(	Timer.nskip[3]	);

#ifdef SIMULATION
        // normally, these parameters are set and defined elsewhere, but we need them during the simulation
        ADD_PTABLE_S(down_home);
        ADD_PTABLE_S(error_code);
        ADD_PTABLE_S(primary_pr);
        // these parameters are only here to pass the mode IDs to Matlab
		ADD_PTABLE_S(_MODE_PROFILE_DOWN);
		ADD_PTABLE_S(_MODE_SETTLE);
		ADD_PTABLE_S(_MODE_PROFILE_UP);
		ADD_PTABLE_S(_MODE_DRIFT_ISO);
		ADD_PTABLE_S(_MODE_DRIFT_SEEK);
		ADD_PTABLE_S(_MODE_DRIFT_ML);
		ADD_PTABLE_S(_MODE_SERVO_P);
		ADD_PTABLE_S(_MODE_BOCHA);
		ADD_PTABLE_S(_NR_REAL_MODES);
		ADD_PTABLE_S(_MODE_GPS);
		ADD_PTABLE_S(_MODE_COMM);
		ADD_PTABLE_S(_MODE_ERROR);
		ADD_PTABLE_S(_MODE_DONE);
        ADD_PTABLE_S(_MODE_START);
		ADD_PTABLE_S(_MODE_SLEEP);
		ADD_PTABLE_S(_MODE_XFER);

		ADD_PTABLE_L(State); // Pass the state to mlf2sim. No need to use it in run-time Ptable (but we may want to export it to ENV some day)

# endif
        /* Mission-specific parameter setup. Also makes Ptable links to Mission-specific parameters */
        initialize_mission_parameters(); // the function is included in Initialize<MISSION>.c

#ifdef SIMULATION
        /* dump Ptable (normally this is done in mission.c, but we'd want this to work in simulator) */
        {
            FILE* f = fopen("param.xml","wt");
            dump_params(f);
            fclose(f);
        }
# endif
            }

#ifdef SIMULATION
			/* ___________________________cut here_________________________________*/
			/*  MATLAB INTERFACE ROUTINE - IGNORE FOR FLOAT INSTALLATION */
			void mexFunction(
				int nlhs, mxArray *plhs[],
				int nrhs, const mxArray *prhs[]
				)
			{
				double
					*day, *P, *T, *S, *T2, *S2, *B_in, *mode_in, *drogue_in, *daysec0, *command_in;
				double
					*B, *mode_out, *drogue_out, *telem_out;
				int           i_mode_out, i_drogue_out;


				/* Check for proper number of arguments */

				if (nrhs < 11) {
					mexErrMsgTxt("set_ballast requires at least 11 input arguments.");
				}
				else if (nlhs <4) {
					mexErrMsgTxt(
						"set_ballast requires 4 or 5 output arguments.");
				}

				/*
				* Create a matrix for the return argument */

				B_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
				M_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
				D_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
				T_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);

				/* Assign pointers to the various parameters */

				B = mxGetPr(B_OUT);
				mode_out = mxGetPr(M_OUT);
				drogue_out = mxGetPr(D_OUT);
				telem_out = mxGetPr(T_OUT);
				day = mxGetPr(DAY_IN);
				P = mxGetPr(P_IN);
				T = mxGetPr(T_IN);
				S = mxGetPr(S_IN);
				T2 = mxGetPr(T2_IN);
				S2 = mxGetPr(S2_IN);
				B_in = mxGetPr(B_IN);
				mode_in = mxGetPr(M_IN);
				drogue_in = mxGetPr(D_IN);
				daysec0 = mxGetPr(DSEC_IN);
				command_in = mxGetPr(CMD_IN);
				/* In real code, INITFUNC=init_mode_params is defined as a "constructor" function and run once on module loading.
				In Matlab simulation we need to call it manually to initialize the Ptable:  */
				if (init_param_table() == 1) {
					init_mode_params();
				}
				/* copy values from Matlab's Ptable */
				if (nrhs > 11) {
					matlab2ptable(PTABLE_IN);
				}

				/* Do the actual computations in a subroutine */

				mlf2_ballast(*day, *P, *T, *S, *T2, *S2, *B_in,
					(int)*mode_in, (int)*drogue_in, *daysec0, (int)*command_in,
					B, &i_mode_out, &i_drogue_out, telem_out);
				/* call MODE_ERROR handling function, normally done in mlf2_main_loop */
				if (i_mode_out == MODE_ERROR)
				{
					i_mode_out = handle_mode_error(get_param_as_int("error_code"));
				}
				*mode_out = i_mode_out;
				*drogue_out = i_drogue_out;
				if (nlhs>4) {
					// return Ptable as a Matlab cell array
					PTABLE_OUT = ptable2matlab(); // should return mxArray*
				}
				return;
			}

			/*
			* Log ballasting diagnostic data -- Matlab version
			*/
			void ballast_log(const char *fmt, ...)
			{
				va_list     args;
				static short bal_records = 0;
				static char bal_filename[] = "bal00001.txt";
				FILE        *ofp;

				va_start(args, fmt);

				if (bal_records == 0)
				{
					// clear if present
					if ((ofp = fopen(bal_filename, "r")))
					{
						fclose(ofp);
						log_event("Clearing ballast file\n");
						ofp = fopen(bal_filename, "w");
						fclose(ofp);
					}
				}

				if ((ofp = fopen(bal_filename, "a")) != NULL)
				{
					if (bal_records == 0)
					{
						fprintf(ofp, BALLAST_LOG_HEADER);
					}
					vfprintf(ofp, fmt, args);
					fclose(ofp);
				}
				else
					log_event("Cannot open ballasting file\n");
				bal_records++;
				va_end(args);
			}

# else // (!SIMULATION)

			/*
			* Log ballasting diagnostic data.
			*/
			void
				ballast_log(const char *fmt, ...)
			{
				va_list     args;
				static short bal_records = 0, bal_max_records = 1000;
				static short bal_file_index = 0;
				static char bal_filename[16];
				FILE        *ofp;

				va_start(args, fmt);

				if (bal_records > bal_max_records || bal_records == 0)
				{
					bal_file_index++;
					sprintf(bal_filename, "bal%05d.txt", bal_file_index);
					if (fileexists(bal_filename))
						unlink(bal_filename);
					bal_records = 0;
				}

				if ((ofp = fopen(bal_filename, "a")) != NULL)
				{
					if (bal_records == 0)
					{
						fprintf(ofp, BALLAST_LOG_HEADER);
					}
					vfprintf(ofp, fmt, args);
					fclose(ofp);
					
				}
				else
					log_error("mission",
						"Cannot open ballasting file\n");
				bal_records++;
				va_end(args);
			}
# endif

/*******************************/
/*     check global timers     */
/*******************************/
int check_timers(int mode, double day_time, double daysec, double last_daysec, int *mode_out)
{
    int result = 0;
	int k;
    if (Timer.enable == 0)
    { // All timers disabled, do nothing
        return 0;
    }

    // timers enabled, let's check them!
    // Timer.timeX needs to occur between last_daysec and daysec for timer X to trigger.
	for (k = 0; k < NTIMERS; k++) {
		if (daysec >= Timer.time[k] && last_daysec < Timer.time[k] && Timer.command[k] >= 0) {
			if (Timer.countdown[k] == 0) {   // Is this a multiday timer?
				log_event("Timer #%d (%5.0fsod) -> Timercommand %d\n",k,daysec,Timer.command[k]);
				result = handle_timer(mode, day_time, Timer.command[k], mode_out);
				Timer.countdown[k] = Timer.nskip[k]; // reset
				break;
			}
			else { /* Yes, multiday time - skip this time */
				log_event("Timer #%d (%5.0fsod) Skip. Countdown %d\n", k,daysec,Timer.countdown[k]);
				Timer.countdown[k] = Timer.countdown[k] - 1;  /* decrement multiday index */
			}
		}
	}
return result;
}
